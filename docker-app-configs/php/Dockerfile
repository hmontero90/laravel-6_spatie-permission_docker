FROM php:7.4-fpm

# Argumentos definidos en docker-compose.yml
ARG user
ARG uid

# Instalacion de dependencias del sistema
RUN apt-get update
RUN apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libpq-dev \
        libonig-dev \
        libxml2-dev \
        libzip-dev \
        unzip \
        nano \
        curl \
        locales

# Limpia las dependencias descargadas
# y elimina paquetes que ya no son necesarios
RUN apt-get clean && rm -rf /var/lib/apt/lists/*


# Instalacion de extensiones del PHP
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install pdo pdo_pgsql mbstring exif pcntl bcmath gd zip

# Copia Composer a nuestro servicio
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Genera 'locale' para es_AR
RUN echo "es_AR.UTF-8 UTF-8" >> /etc/locale.gen
RUN locale-gen
RUN echo "es      es_AR.UTF-8" >> /etc/locale.alias

# Crea nuestro usuario del sistema
# y le agrega los permisos correspondientes
RUN useradd -G www-data,root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user/.composer && \
    chown -R $user:$user /home/$user

# Lleva a la carpeta de trabajo
WORKDIR /app/

USER $user