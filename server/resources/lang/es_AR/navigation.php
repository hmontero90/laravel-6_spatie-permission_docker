<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Translations for single words.
    |--------------------------------------------------------------------------
    */
    'sites'     => 'Sitios',
    'site'      => 'Sitio',
    'users'     => 'Usuarios',
    'articles'  => 'Noticias',
    'roles'     => 'Roles',
    'categories'=> 'Categorias',
    'pages'     => 'Páginas',
    'documents' => 'Documentos',
    'highlights' => 'Destacados',
    'new'       => 'Nuevo',
    'list'      => 'Listado',

];
