<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Mensajes personalizados para distintas acciones
    |--------------------------------------------------------------------------
    */
    'store-success'   => 'Se guardó correctamente.',
    'update-success'  => 'Se actualizó correctamente.',
    'delete-success'  => 'Se eliminó correctamente.',
    'delete-error'    => 'Error al eliminar.',
    'restore-success' => 'Se restableció correctamente.',
    'mail-sent'       => 'El mensaje se envió correctamente',
    'mail-sent-fail'  => 'No se pudo enviar tu mensaje. Por favor intentá nuevamente.',
    'resource-old'    => 'El recurso se encuentra desactualizado',

    'users' => [
        // Success
	    'store-success'      => 'Se guardó correctamente el usuario :email.',
	    'update-success'     => 'Se actualizó correctamente el usuario :email.',
	    'delete-success'     => 'Se eliminó correctamente el usuario :email.',
	    'restore-success'    => 'Se reactivó correctamente el usuario :email.',
        'confirm-success'    => 'Se confirmó la cuenta correctamente. Por favor acceda con sus nuevas credenciales.',
        'email-sent-success' => 'Hemos enviado un correo a tu cuenta.',

        // Errors
        'email-not-found'    => 'No se ha encontrado la cuenta. Por favor ingrese una cuenta de correo válida.',
        'restricted-area'    => 'No tiene permisos para acceder a la página solicitada.'
    ],

    'uploadError' => 'Error de subida.'
];
