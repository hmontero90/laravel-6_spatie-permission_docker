<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    'accepted' => 'El :attribute debe ser aceptado.',
    'active_url' => 'El :attribute no es una URL válida.',
    'after' => 'El :attribute debe ser una fecha después de :date.',
    'alpha' => 'El :attribute solo puede contener letras.',
    'alpha_dash' => 'El :attribute solo puede contener letras, números y guiones.',
    'alpha_num' => 'El :attribute solo puede contener letras y números.',
    'array' => 'El :attribute debe ser una matriz.',
    'before' => 'El :attribute debe ser una fecha anterior a :date.',
    'between' => [
        'numeric' => 'El :attribute debe estar entre :min y :max.',
        'file' => 'El :attribute debe estar entre :min y :max kilobytes.',
        'string' => 'El :attribute debe estar entre :min y :max characters.',
        'array' => 'El :attribute debe tener entre :min y :max artículos.',
    ],
    'boolean' => 'El :attribute debe ser verdadero o falso.',
    'confirmed' => 'La confirmación de :attribute no coincide.',
    'date' => 'El :attribute no es una fecha válida.',
    'date_format' => 'El :attribute no coincide con el formato :format.',
    'different' => 'El :attribute y :other deben ser diferentes.',
    'digits' => 'El :attribute debe ser :digits dígitos.',
    'digits_between' => 'El :attribute debe estar entre :min y :max dígitos.',
    'email' => 'El :attribute debe ser una dirección de correo electrónico válida.',
    'exists' => 'El :attribute seleccionado no es válido.',
    'filled' => 'El :attribute es obligatorio.',
    'image' => 'El :attribute debe ser una imagen.',
    'in' => 'El :attribute seleccionado no es válido.',
    'integer' => 'El :attribute debe ser un entero.',
    'ip' => 'El :attribute debe ser una dirección IP válida.',
    'json' => 'El :attribute debe ser una cadena JSON válida.',
    'max' => [
        'numeric' => 'El :attribute no puede ser mayor que :max.',
        'file' => 'El :attribute no puede ser mayor que :max kilobytes.',
        'string' => 'El :attribute debe tener un máximo de :max caracteres.',
        'array' => 'El :attribute no puede tener más de :max elementos.',
    ],
    'mimes' => 'El :attribute debe ser un archivo de tipo :values.',
    'min' => [
        'numeric' => 'El :attribute debe ser al menos :min.',
        'file' => 'El :attribute debe ser al menos :min kilobytes.',
        'string' => 'El :attribute debe tener un mínimo de :min caracteres.',
        'array' => 'El :attribute debe tener al menos :min elementos.',
    ],
    'not_in' => 'El :attribute seleccionado no es válido.',
    'numeric' => 'El :attribute debe ser un número.',
    'regex' => 'El :attribute tiene un formato no es válido.',
    'required' => 'El :attribute es obligatorio.',
    'required_if' => 'El :attribute es obligatorio cuando :other es :value.',
    'required_unless' => 'El :attribute es obligatorio a menos que :other es en :values.',
    'required_with' => 'El :attribute es obligatorio cuando :values está presente.',
    'required_with_all' => 'El :attribute es obligatorio cuando :values estan presente.',
    'required_without' => 'El :attribute es obligatorio cuando :values no están presentes.',
    'required_without_all' => 'El campo :attribute es obligatorio cuando ninguno de los :values está presente.',
    'same' => 'El :attribute y :other deben coincidir.',
    'size' => [
        'numeric' => 'El :attribute debe ser :size.',
        'file' => 'El :attribute debe ser :size kilobytes.',
        'string' => 'El :attribute debe ser :size caracteres.',
        'array' => 'El :attribute debe contener :size elementos.',
    ],
    'string' => 'El :attribute debe ser una cadena.',
    'timezone' => 'El :attribute debe ser una zona válida.',
    'unique' => 'El :attribute ya se ha tomado.',
    'url' => 'El :attribute tiene un formato no válido.',

    'tag_permission' => 'Este usuario no tiene permisos para agregar nuevas etiquetas.',

    'unique_not_deleted' => 'El :attribute ya se ha tomado.',

    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */

    'custom' => [
        'name' => [
            'required'          => 'El nombre es obligatorio.',
            'min'               => 'El nombre no puede contener menos de :min caracteres.',
            'max'               => 'El nombre no puede superar los :max caracteres.',
            'unique_per_site'   => 'El nombre elegido no se encuentra disponible.'
        ],
        'slug' => [
            'required'          => 'El nombre amigable para URL es obligatorio.',
            'unique'            => 'El nombre amigable para URL elegido no se encuentra disponible.',
        ],
        'email_address' => [
            'email'             => 'La dirección de correo electrónico no parece válida.',
            'required'          => 'La dirección de correo electrónico es obligatoria.',
            'max'               => 'La dirección de correo electrónico no puede superar los :max caracteres.'
        ],
        'email' => [
            'email'             => 'La dirección de correo electrónico no parece válida.',
            'required'          => 'La dirección de correo electrónico es obligatoria.',
            'unique'            => 'La dirección de correo electrónico está ocupada.',
            'max'               => 'La dirección de correo electrónico no puede superar los :max caracteres.'
        ],
        'password' => [
            'required'          => 'La contraseña es obligatoria.',
            'min'               => 'La contraseña no puede contener menos de :min caracteres.',
            'max'               => 'La contraseña no puede superar los :max caracteres.',
            'confirmed'         => 'La confirmación de la contraseña no coincide.'
        ],
        'password_confirmation' => [
            'required'          => 'La confirmación de la contraseña es obligatoria.',
            'min'               => 'La confirmación de la contraseña no puede contener menos de :min caracteres.',
            'max'               => 'La confirmación de la contraseña no puede superar los :max caracteres.',
        ],
        'title' => [
            'required'          => 'El título es obligatorio.',
            'max'               => 'El título excede los :max caracteres.'
        ],
        'description' => [
            'required'          => 'La descripción es obligatoria.',
            'max'               => 'La descripción excede los :max caracteres.'
        ],
        'short_description' => [
            'max'               => 'La descripción excede los :max caracteres.'
        ],
        'document' => [
            'required_without' => 'El documento o url documento es obligatorio.',
            'mimes'            => 'El documento tiene que ser de alguno de los siguientes tipos: :values.'
        ],
        'document_url' => [
            'required_without' => 'El documento o url documento es obligatorio.',
            'max'              => 'El documento o URL no puede superar los :max caracteres.'
        ],
        'body' => [
            'required'          => 'El cuerpo es obligatorio.'
        ],
        'category_id' => [
            'required'          => 'La categoría es obligatoria.'
        ],
        'model_id' => [
            'required'          => 'El modelo es obligatorio.'
        ],
        'publish_date' => [
            'required'          => 'La fecha de publicación es obligatoria.'
        ],
        'image' => [
            'required'          => 'La imagen es obligatoria.',
            'mimes'             => 'La imagen tiene que ser de alguno de los siguientes tipos: :values.'
        ],
        'image_alt' => [
            'required'          => 'El texto alternativo es obligatorio cuando se utiliza una imagen.',
            'required_with'     => 'El texto alternativo es obligatorio cuando se utiliza una imagen.',
            'max'               => 'El texto alternativo no puede superar los :max caracteres.'
        ],
        'facebook_url' => [
            'max'               => 'La dirección URL de Facebook excede los :max caracteres.'
        ],
        'twitter_url' => [
            'max'               => 'La dirección URL de Twitter excede los :max caracteres.'
        ],
        'youtube_url' => [
            'max'               => 'La dirección URL de YouTube excede los :max caracteres.'
        ],
        'linkedin_url' => [
            'max'               => 'La dirección URL de LinkedIn excede los :max caracteres.'
        ],
        'order' => [
            'numeric'           => 'El orden debe de ser numérico.'
        ],
        'organization_description' => [
            'required'          => 'La descripción de la organización es obligatoria.'
        ],
        'permissions' => [
            'required'          => 'El permiso es obligatorio.'
        ],
        'year' => [
            'required'          => 'El año es obligatorio.'
        ],
        'categories' => [
            'required'          => 'La categoría es obligatoria.'
        ],
        'url' => [
            'required'          => 'El enlace es obligatorio.',
            'max'               => 'El enlace no puede superar los :max caracteres.'
        ],
        'address' => [
            'required'          => 'La dirección es obligatorio.',
        ],
        'phone' => [
            'required'          => 'El número telefónico es obligatorio.',
        ],
        'roles' => [
            'required'          => 'El rol es obligatorio.',
        ],
    ],

    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => [
        'title' => 'Título'
    ],
];
