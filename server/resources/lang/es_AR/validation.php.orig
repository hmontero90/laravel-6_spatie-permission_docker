<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'between' => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => 'The :attribute is not a valid date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'email' => 'The :attribute must be a valid email address.',
    'exists' => 'The selected :attribute is invalid.',
    'filled' => 'The :attribute field is required.',
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'El :attribute debe tener un máximo de :max caracteres.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'El :attribute debe tener un mínimo de :min caracteres.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'El :attribute es obligatorio.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values is present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'url' => 'The :attribute format is invalid.',

    'tag_permission' => 'Este usuario no tiene permisos para agregar nuevas etiquetas.',



    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */

    'custom' => [
        'name' => [
            'required'      => 'El nombre es obligatorio.',
            'min'           => 'El nombre no puede contener menos de :min caracteres.',
            'max'           => 'El nombre no puede superar los :max caracteres.',
            'unique_per_site' => 'El nombre elegido no se encuentra disponible.'
        ],
        'slug' => [
            'required'      => 'El nombre amigable para URL es obligatorio.',
            'unique'        => 'El nombre amigable para URL elegido no se encuentra disponible.',
        ],
        'email_address' => [
            'email'         => 'La dirección de correo electrónico no parece válida.',
            'required'      => 'La dirección de correo electrónico es obligatoria.',
            'max'           => 'La dirección de correo electrónico no puede superar los :max caracteres.'
        ],
        'email' => [
            'email'         => 'La dirección de correo electrónico no parece válida.',
            'required'      => 'La dirección de correo electrónico es obligatoria.',
            'unique'        => 'La dirección de correo electrónico está ocupada.',
            'max'           => 'La dirección de correo electrónico no puede superar los :max caracteres.'
        ],
        'password' => [
            'required'      => 'La contraseña es obligatoria.',
            'min'           => 'La contraseña no puede contener menos de :min caracteres.',
            'max'           => 'La contraseña no puede superar los :max caracteres.',
            'confirmed'     => 'La confirmación de la contraseña no coincide.'
        ],
        'password_confirmation' => [
            'required'      => 'La confirmación de la contraseña es obligatoria.',
            'min'           => 'La confirmación de la contraseña no puede contener menos de :min caracteres.',
            'max'           => 'La confirmación de la contraseña no puede superar los :max caracteres.',
        ],
        'title' => [
            'required'      => 'El título es obligatorio.',
            'max'           => 'El título excede los :max caracteres.'
        ],
        'description' => [
            'required'      => 'La descripción es obligatoria.',
            'max'           => 'La descripción excede los :max caracteres.'
        ],
        'short_description' => [
            'max'           => 'La descripción excede los :max caracteres.'
        ],
        'document' => [
            'required_without' => 'El documento o url documento es obligatorio.',
            'mimes'            => 'El documento tiene que ser de alguno de los siguientes tipos: :values.'
        ],
        'document_url' => [
            'required_without' => 'El documento o url documento es obligatorio.',
            'max'              => 'El documento o URL no puede superar los :max caracteres.'
        ],
        'body' => [
            'required'      => 'El cuerpo es obligatorio.'
        ],
        'category_id' => [
            'required'      => 'La categoría es obligatoria.'
        ],
        'model_id' => [
            'required'      => 'El modelo es obligatorio.'
        ],
        'publish_date' => [
            'required'      => 'La fecha de publicación es obligatoria.'
        ],
        'image' => [
            'required'       => 'La imagen es obligatoria.',
            'mimes'          => 'La imagen tiene que ser de alguno de los siguientes tipos: :values.'
        ],
        'image_alt' => [
            'required'       => 'El texto alternativo es obligatorio cuando se utiliza una imagen.',
            'required_with'  => 'El texto alternativo es obligatorio cuando se utiliza una imagen.',
            'max'            => 'El texto alternativo no puede superar los :max caracteres.'
        ],
        'facebook_url' => [
            'max'            => 'La dirección URL de Facebook excede los :max caracteres.'
        ],
        'twitter_url' => [
            'max'            => 'La dirección URL de Twitter excede los :max caracteres.'
        ],
        'youtube_url' => [
            'max'            => 'La dirección URL de YouTube excede los :max caracteres.'
        ],
        'linkedin_url' => [
            'max'            => 'La dirección URL de LinkedIn excede los :max caracteres.'
        ],
        'order' => [
            'numeric'        => 'El orden debe de ser numérico.'
        ],
        'organization_description' => [
            'required'       => 'La descripción de la organización es obligatoria.'
        ],
        'permissions' => [
            'required'       => 'El permiso es obligatorio.'
        ],
        'year' => [
            'required'       => 'El año es obligatorio.'
        ],
        'categories' => [
            'required'      => 'La categoría es obligatoria.'
        ],
        'url' => [
            'required' => 'El enlace es obligatorio.',
            'max'              => 'El enlace no puede superar los :max caracteres.'
        ],
        'address' => [
            'required' => 'La dirección es obligatorio.',
        ],
        'phone' => [
            'required' => 'El número telefónico es obligatorio.',
        ],
        'roles' => [
            'required' => 'El rol es obligatorio.',
        ],
    ],

    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => [
        'title' => 'Título'
    ],
];
