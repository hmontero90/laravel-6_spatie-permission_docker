<div class="form-group row">
    <label for="password"
           class="col-md-4 col-form-label text-md-right">
        Contraseña:
    </label>

    <div class="col-md-6">
        <input id="password"
               type="password"
               class="form-control @error('password') is-invalid @enderror"
               name="password"
               value="{{ old('password') }}"
               title="Contraseña"
               required
               autocomplete="new-password">

        @error('password')
        <span class="invalid-feedback"
              role="alert">
            <strong>
                {{ $message }}
            </strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="password-confirm"
           class="col-md-4 col-form-label text-md-right">
        Confirmar contraseña:
    </label>

    <div class="col-md-6">
        <input id="password-confirm"
               type="password"
               class="form-control"
               name="password_confirmation"
               value="{{ old('password_confirmation') }}"
               title="Confirmar contraseña"
               required
               autocomplete="new-password">
    </div>
</div>

