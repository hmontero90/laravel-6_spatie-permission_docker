<div class="form-group row">
    <label for="avatar"
           class="col-md-4 col-form-label text-md-right">
    </label>
    <div class="col-md-6">
        @if(isset(auth()->user()->avatar))
            <img class="img-profile rounded-circle"
                 id="avatar"
                 src="/usuarios/perfil/avatar"
                 alt="Imagen del perfil"
                 style="width:20%">
            <br/>
            <a href="/usuarios/perfil/eliminar/avatar">
                Eliminar imagen
            </a>
        @else
            <img class="img-profile rounded-circle"
                 id="avatar"
                 src="/img/photo.jpg"
                 alt="Imagen del perfil"
                 style="width:20%">
            <br/>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="avatar"
           class="col-md-4 col-form-label text-md-right">
        Imagen del perfil:
    </label>

    <div class="col-md-6">
        <input id="avatar"
               type="file"
               class="form-control @error('avatar') is-invalid @enderror"
               name="avatar"
               title="Imagen del perfil"
               value="{{ $user->avatar }}"
               accept="image/*"
               onchange="loadFile(event)">

        @error('avatar')
        <span class="invalid-feedback"
              role="alert">
            <strong>
                {{ $message }}
            </strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="name"
           class="col-md-4 col-form-label text-md-right">
        Nombre:
    </label>

    <div class="col-md-6">
        <input id="name"
               type="text"
               class="form-control @error('name') is-invalid @enderror"
               name="name"
               title="Nombre"
               value="{{ $user->name }}"
               required
               autocomplete="name"
               autofocus>

        @error('name')
        <span class="invalid-feedback"
              role="alert">
            <strong>
                {{ $message }}
            </strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="email"
           class="col-md-4 col-form-label text-md-right">
        Correo electrónico:
    </label>

    <div class="col-md-6">
        <input id="email"
               type="email"
               class="form-control @error('email') is-invalid @enderror"
               name="email"
               title="Correo electrónico"
               value="{{ $user->email }}"
               required
               autocomplete="email">

        @error('email')
        <span class="invalid-feedback"
              role="alert">
            <strong>
                {{ $message }}
            </strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
        @include('layouts.shared.button.cancel', [
            'route' => '/home'
        ])
        @include('layouts.shared.button.submit')
    </div>
</div>

<script>
    var loadFile = function(event) {
        var reader = new FileReader();
        reader.onload = function(){
            var output = document.getElementById('avatar');
            output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };
</script>
