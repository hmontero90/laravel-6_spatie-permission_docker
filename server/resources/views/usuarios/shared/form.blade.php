<div class="form-group row">
    <label for="name"
           class="col-md-4 col-form-label text-md-right">
        Nombre:
    </label>

    <div class="col-md-6">
        <input id="name"
               type="text"
               class="form-control @error('name') is-invalid @enderror"
               name="name"
               title="Nombre"
               @if(isset($user))
               value="{{ $user->name }}"
               @else
               value="{{ old('name') }}"
               @endif
               required
               autocomplete="name"
               autofocus>

        @error('name')
        <span class="invalid-feedback"
              role="alert">
            <strong>
                {{ $message }}
            </strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="email"
           class="col-md-4 col-form-label text-md-right">
        Correo electrónico:
    </label>

    <div class="col-md-6">
        <input id="email"
               type="email"
               class="form-control @error('email') is-invalid @enderror"
               name="email"
               title="Correo electrónico"
               @if(isset($user))
               value="{{ $user->email }}"
               @else
               value="{{ old('email') }}"
               @endif
               required
               autocomplete="email">

        @error('email')
        <span class="invalid-feedback"
              role="alert">
            <strong>
                {{ $message }}
            </strong>
        </span>
        @enderror
    </div>
</div>

@if(!isset($user))
<div class="form-group row">
    <label for="password"
           class="col-md-4 col-form-label text-md-right">
        Contraseña:
    </label>

    <div class="col-md-6">
        <input id="password"
               type="password"
               class="form-control @error('password') is-invalid @enderror"
               name="password"
               title="Contraseña"
               required
               autocomplete="new-password">

        @error('password')
        <span class="invalid-feedback"
              role="alert">
            <strong>
                {{ $message }}
            </strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="password-confirm"
           class="col-md-4 col-form-label text-md-right">
        Confirmar contraseña:
    </label>

    <div class="col-md-6">
        <input id="password-confirm"
               type="password"
               class="form-control"
               name="password_confirmation"
               title="Confirmar contraseña"
               required
               autocomplete="new-password">
    </div>
</div>
@endif

<div class="form-group row">
    <label for="roles"
           class="col-md-4 col-form-label text-md-right">
        Role:
    </label>
    <div class="col-md-6">
    @if(isset($roles))
        @if(isset($user))
            @php
                $userRoleName = $user->roles->first()->name;
            @endphp
            @foreach($roles as $role)
                @if($role->name == $userRoleName)
                <input type="radio"
                       name="roles[]"
                       title="{{$role->display_name}}"
                       value="{{$role->name}}"
                       checked>
                <label>{{$role->display_name}}</label>
                <br/>
                @else
                <input type="radio"
                       name="roles[]"
                       title="{{$role->display_name}}"
                       value="{{$role->name}}">
                <label>{{$role->display_name}}</label>
                <br/>
                @endif
            @endforeach
        @else
            @foreach($roles as $key => $role)
                @if($key === 0)
                    <input type="radio"
                           name="roles[]"
                           title="{{$role->display_name}}"
                           value="{{$role->name}}"
                            checked>
                    <label>{{$role->display_name}}</label>
                    <br/>
                @else
                    <input type="radio"
                           name="roles[]"
                           title="{{$role->display_name}}"
                           value="{{$role->name}}">
                    <label>{{$role->display_name}}</label>
                    <br/>
                @endif
            @endforeach
        @endif
    @endif
    </div>
</div>

<div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
        @include('layouts.shared.button.cancel', [
            'route' => '/usuarios'
        ])
        @include('layouts.shared.button.submit')
    </div>
</div>
