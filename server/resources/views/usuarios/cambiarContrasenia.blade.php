@extends('layouts.app')

@section('title', 'Editar usuario')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-12">

                <!-- Div Titulo -->
                <div class="card-header py-3">
                    <div class="float-left">
                        <h5 class="m-0 font-weight-bold text-primary">
                            Cambiar contraseña del usuario: "{{$user->email}}"
                        </h5>
                    </div>
                    <div class="float-right">
                    </div>
                </div>
                <!-- Fin Div Titulo -->

                <!-- Div Contenido -->
                <div class="card-body">

                    <!-- Formulario -->
                    <form action="/usuarios/cambiar/contrasenia/{{$user->id}}"
                          method="POST">
                        @csrf

                        <!-- Formulario cambiar contraseña -->
                        @include('usuarios.shared.form_changePassword')
                        <!-- Fin Formulario cambiar contraseña -->

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                @include('layouts.shared.button.cancel', [
                                    'route' => '/usuarios'
                                ])
                                @include('layouts.shared.button.submit')
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
