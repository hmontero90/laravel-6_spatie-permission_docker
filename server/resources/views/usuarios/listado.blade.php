@extends('layouts.app')

@section('title', 'Listado de usuarios')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card shadow mb-12">

            <!-- Div Titulo -->
            <div class="card-header py-3">
                <div class="float-left">
                    <h5 class="m-0 font-weight-bold text-primary">
                        Listado de los usuarios
                    </h5>
                </div>
                <div class="float-right">
                    @include('layouts.shared.button.add', [
                            'permiso_agregar' => 'create_user',
                            'route' => '/usuarios/crear'
                        ])
                </div>
            </div>
            <!-- Fin Div Titulo -->

            <!-- Div Contenido -->
            <div class="card-body">

                <!-- Listado de los usuarios -->
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-sm display"
                           style="width:100%"
                           id="users-table">
                        <thead class="thead-dark">
                        <th class="text-center" style="width:10px;">ID</th>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Correo Electrónico</th>
                        <th class="text-center">Role</th>
                        <th class="text-center" style="width:10px;">Acciones</th>
                        </thead>
                    </table>
                </div>
                <!-- Fin Listado de los usuarios -->

            </div>
            <!-- Fin Div Contenido -->

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function() {
        $('#users-table').DataTable({
            ajax: '/usuarios/datos',
            columns: [
                {data: 'id', name: 'id', visible: true},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'roles', name: 'roles'},
                {data: 'actions', name: 'actions', orderable:false}
            ],
        });
    });
</script>
@endsection
