@extends('layouts.app')

@section('title', 'Perfil del usuario')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-12">

                <!-- Div Titulo -->
                <div class="card-header py-3">
                    <div class="float-left">
                        <h5 class="m-0 font-weight-bold text-primary">
                            Perfil del usuario: "{{$user->name}}"
                        </h5>
                    </div>
                    <div class="float-right">
                    </div>
                </div>
                <!-- Fin Div Titulo -->

                <!-- Div Contenido -->
                <div class="card-body">

                    <!-- Formulario -->
                    <form action="/usuarios/perfil"
                          method="POST"
                          enctype="multipart/form-data">
                        @csrf

                        <!-- Formulario del perfil -->
                        @include('usuarios.shared.form_profile')
                        <!-- Fin Formulario del perfil -->

                    </form>
                    <!-- Fin Formulario -->

                </div>
                <!-- Fin Div Contenido -->

            </div>
        </div>
    </div>

@endsection
