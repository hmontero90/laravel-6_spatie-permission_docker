@extends('layouts.app')

@section('title', 'Editar usuario')

@section('content')

    <!-- Formulario -->
    <form action="/usuarios/editar/{{$user->id}}"
          method="POST">
        @csrf

        @include('usuarios.shared.form')

    </form>

@endsection
