@extends('layouts.app')

@section('title', 'Ver usuario')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-12">

                <!-- Div Titulo -->
                <div class="card-header py-3">
                    <div class="float-left">
                        <h5 class="m-0 font-weight-bold text-primary">
                            Ver usuario
                        </h5>
                    </div>
                    <div class="float-right">
                        <!-- -->
                    </div>
                </div>
                <!-- Fin Div Titulo -->

                <!-- Div Contenido -->
                <div class="card-body">
                    <div class="form-group row">
                        <label for="name"
                               class="col-md-4 col-form-label text-md-right">
                            Nombre:
                        </label>

                        <div class="col-md-6">
                            <input id="name"
                                   type="text"
                                   class="form-control"
                                   name="name"
                                   title="Nombre"
                                   @if(isset($user))
                                   value="{{ $user->name }}"
                                   @endif
                                   disabled>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email"
                               class="col-md-4 col-form-label text-md-right">
                            Correo electrónico:
                        </label>

                        <div class="col-md-6">
                            <input id="email"
                                   type="email"
                                   class="form-control"
                                   name="email"
                                   title="Correo electrónico"
                                   @if(isset($user))
                                   value="{{ $user->email }}"
                                   @endif
                                   disabled>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="roles"
                               class="col-md-4 col-form-label text-md-right">
                            Role:
                        </label>
                        <div class="col-md-6">
                            <input type="radio"
                                   name="roles[]"
                                   title="{{$user->roles->first()->display_name}}"
                                   value="{{$user->roles->first()->name}}"
                                   checked
                                   disabled>
                            <label>{{$user->roles->first()->display_name}} </label>
                            <br/>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            @include('layouts.shared.button.cancel', [
                                'route' => '/usuarios'
                            ])
                        </div>
                    </div>

                </div>
                <!-- Fin Div Contenido -->

            </div>
        </div>
    </div>
@endsection
