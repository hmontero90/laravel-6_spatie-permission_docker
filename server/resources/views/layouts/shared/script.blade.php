<!-- Bootstrap core JavaScript-->
<script type="text/javascript" src="/bootstrap/vendor/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script type="text/javascript" src="/bootstrap/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script type="text/javascript" src="/bootstrap/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
{{--
<script src="/bootstrap/vendor/chart.js/Chart.min.js"></script>
--}}

<!-- Page level custom scripts -->
{{--<script src="/bootstrap/js/demo/chart-area-demo.js"></script>
<script src="/bootstrap/js/demo/chart-pie-demo.js"></script>--}}

<!-- DataTable-->
<script type="text/javascript" src="/bootstrap/vendor/datatables/jquery.dataTables.js"></script>
<script type="text/javascript" src="/bootstrap/vendor/datatables/dataTables.bootstrap4.js"></script>
<script type="text/javascript" src="/bootstrap/vendor/datatables/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="/bootstrap/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script type="text/javascript" src="/bootstrap/vendor/datatables/buttons.colVis.min.js"></script>
<script type="text/javascript" src="/bootstrap/vendor/datatables/buttons.flash.min.js"></script>
<script type="text/javascript" src="/bootstrap/vendor/datatables/buttons.html5.min.js"></script>
<script type="text/javascript" src="/bootstrap/vendor/datatables/buttons.print.min.js"></script>
<script type="text/javascript" src="/bootstrap/vendor/datatables/jszip.min.js"></script>
<script type="text/javascript" src="/bootstrap/vendor/datatables/pdfmake.min.js"></script>
<script type="text/javascript" src="/bootstrap/vendor/datatables/vfs_fonts.js"></script>
<script type="text/javascript" src="/bootstrap/dataTable/config.js"></script>
