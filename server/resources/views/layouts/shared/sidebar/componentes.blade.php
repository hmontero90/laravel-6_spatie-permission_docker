<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item {{ Request::is('notas') ? 'active' : '' }}">
    <a class="nav-link collapsed"
       href="#"
       data-toggle="collapse"
       data-target="#collapseTwo"
       aria-expanded="true"
       aria-controls="collapseTwo"
    >
        <i class="fas fa-fw fa-cog"></i>
        <span>
            Componentes
        </span>
    </a>
    <div id="collapseTwo"
         class="collapse {{ Request::is('notas') ? 'show' : '' }}
                         {{ Request::is('notas/papelera') ? 'show' : '' }}"
         aria-labelledby="headingTwo"
         data-parent="#accordionSidebar"
    >
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">
                Personalizar componentes
            </h6>
            <a class="collapse-item {{ Request::is('notas') ? 'active' : '' }}"
               href="/notas">
                <i class="fas fa-clipboard-list"> </i>
                Notas
            </a>
            <a class="collapse-item {{ Request::is('notas/papelera') ? 'active' : '' }}"
               href="/notas/papelera"
            >
                <i class="fa fa-trash"> </i>
                Papelera
            </a>
        </div>
    </div>
</li>
