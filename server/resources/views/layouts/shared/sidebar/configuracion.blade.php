<!-- Nav Item - Utilities Collapse Menu -->
@can(['view_user','view_role'])
    <li class="nav-item {{ Request::is('usuarios') ? 'active' : '' }}">
        <a class="nav-link collapsed"
           href="#"
           data-toggle="collapse"
           data-target="#collapseUsers"
           aria-expanded="true"
           aria-controls="collapseUsers"
        >
            <i class="fas fa-fw fa-user"></i>
            <span>
                Usuarios
            </span>
        </a>
        <div id="collapseUsers"
             class="collapse
             {{ Request::is('usuarios') ? 'show' : '' }}
             {{ Request::is('usuarios/papelera') ? 'show' : '' }}"
             aria-labelledby="headingUtilities"
             data-parent="#accordionSidebar"
        >
            <div class="bg-white py-2 collapse-inner rounded">
                @can('view_user')
                    @can('view_user')
                        <a class="collapse-item {{ Request::is('usuarios') ? 'active' : '' }}"
                           href="/usuarios"
                        >
                            <i class="fa fa-list"> </i>
                            Listado
                        </a>
                        @can('delete_user')
                            <a class="collapse-item {{ Request::is('usuarios/papelera') ? 'active' : '' }}"
                               href="/usuarios/papelera"
                            >
                                <i class="fa fa-trash"> </i>
                                Papelera
                            </a>
                        @endcan
                    @endcan
                @endcan
            </div>
        </div>
    </li>

    <li class="nav-item {{ Request::is('roles') ? 'active' : '' }}">
        <a class="nav-link collapsed"
           href="#"
           data-toggle="collapse"
           data-target="#collapseRoles"
           aria-expanded="true"
           aria-controls="collapseRoles"
        >
            <i class="fas fa-fw fa-key"></i>
            <span>
                Roles
            </span>
        </a>
        <div id="collapseRoles"
             class="collapse
             {{ Request::is('roles') ? 'show' : '' }}
             {{ Request::is('roles/papelera') ? 'show' : '' }}"
             aria-labelledby="headingUtilities"
             data-parent="#accordionSidebar"
        >
            <div class="bg-white py-2 collapse-inner rounded">
                @can('view_role')
                    @can('view_role')
                        <a class="collapse-item {{ Request::is('roles') ? 'active' : '' }}"
                           href="/roles">
                            <i class="fa fa-list"> </i>
                            Listado
                        </a>
                        @can('delete_role')
                            <a class="collapse-item {{ Request::is('roles/papelera') ? 'active' : '' }}"
                               href="/roles/papelera"
                            >
                                <i class="fa fa-trash"> </i>
                                Papelera
                            </a>
                        @endcan
                    @endcan
                @endcan
            </div>
        </div>
    </li>
@endcan
