@if(isset($permiso_agregar))
    @can($permiso_agregar)
        <a href="{{$route}}"
           class="btn btn-success btn-icon-split">
            <span class="icon text-white-50">
              <i class="fas fa-plus"></i>
            </span>
            <span class="text">
                Agregar
            </span>
        </a>
    @endcan
@endif
