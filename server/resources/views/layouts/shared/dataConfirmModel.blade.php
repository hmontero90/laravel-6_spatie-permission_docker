<div class="modal fade"
     id="dataConfirmModal"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog"
         role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"
                    id="myModalLabel">
                    ...
                </h4>
                <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <a type="button"
                   class="btn btn-primary button-confirm"
                   href="#">
                    Confirmar
                </a>
                <button type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal">
                    Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
