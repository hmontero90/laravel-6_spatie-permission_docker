@if(isset($permiso_eliminar))
    @can($permiso_eliminar)
        <a class="dropdown-item"
           title="Eliminar"
           href="#"
           data-toggle="modal"
           data-target="#dataConfirmModal"
           data-title="Eliminar"
           data-confirm="¿Estás seguro de que quieres eliminar?"
           data-href="{{$url_eliminar}}"
        >
            <i class="fa fa-trash"> Eliminar</i>
        </a>
    @else
        <a class="dropdown-item"
           title="No tiene permiso"
        >
            <i class="fa fa-frown"> Eliminar</i>
        </a>
    @endcan
@endif

<script>
    $(document).ready(function() {
        $('a[data-confirm]').click(function(ev) {
            var title = $(this).attr('data-title');
            var body = $(this).attr('data-confirm');
            var href = $(this).attr('data-href');
            $("#dataConfirmModal").find(".modal-title").text(title);
            $('#dataConfirmModal').find('.modal-body').text(body);
            $('#dataConfirmModal').find('.button-confirm').attr('href', href);
            $('#dataConfirmModal').modal({show:true});
            return false;
        });
    });
</script>
