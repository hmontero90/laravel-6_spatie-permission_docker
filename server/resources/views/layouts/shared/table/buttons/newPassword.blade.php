@if(isset($permiso_cambiarContrasenia))
    @can($permiso_cambiarContrasenia)
        <a class="dropdown-item"
           title="Cambiar contraseña"
           href="{{$url_cambiarContrasenia}}"
        >
            <i class="fa fa-unlock-alt"> Cambiar contraseña</i>
        </a>
    @else
        <a class="dropdown-item"
           title="No tiene permiso"
        >
            <i class="fa fa-frown"> Cambiar contraseña</i>
        </a>
    @endcan
@endif
