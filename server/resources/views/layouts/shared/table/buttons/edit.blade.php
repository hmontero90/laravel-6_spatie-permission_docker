@if(isset($permiso_editar))
    @can($permiso_editar)
        <a class="dropdown-item"
           title="Editar"
           href="{{$url_editar}}"
        >
            <i class="fa fa-pencil-alt"> Editar</i>
        </a>
    @else
        <a class="dropdown-item"
           title="No tiene permiso"
        >
            <i class="fa fa-frown"> Editar</i>
        </a>
    @endcan
@endif
