@if(isset($permiso_ver))
    @can($permiso_ver)
        <a class="dropdown-item"
           title="Detalle"
           href="{{$url_ver}}"
        >
            <i class="fa fa-eye"> Ver</i>
        </a>
    @else
        <a class="dropdown-item"
           title="No tiene permiso"
        >
            <i class="fa fa-frown-o"> Ver</i>
        </a>
    @endcan
@endif
