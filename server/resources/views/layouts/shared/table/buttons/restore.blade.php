@if(isset($permiso_restaurar))
    @can($permiso_restaurar)
        <a class="dropdown-item"
           title="Restaurar"
           href="#"
           data-toggle="modal"
           data-target="#dataConfirmModal"
           data-title="Restaurar"
           data-confirm="¿Estás seguro de que quieres recuperar?"
           data-href="{{$url_restaurar}}"
        >
            <i class="fa fa-trash"> Restaurar</i>
        </a>
    @else
        <a class="dropdown-item"
           title="No tiene permiso"
        >
            <i class="fa fa-frown"> Restaurar</i>
        </a>
    @endcan
@endif

<script>
    $(document).ready(function() {
        $('a[data-confirm]').click(function(ev) {
            var title = $(this).attr('data-title');
            var body = $(this).attr('data-confirm');
            var href = $(this).attr('data-href');
            $("#dataConfirmModal").find(".modal-title").text(title);
            $('#dataConfirmModal').find('.modal-body').text(body);
            $('#dataConfirmModal').find('.button-confirm').attr('href', href);
            $('#dataConfirmModal').modal({show:true});
            return false;
        });
    });
</script>
