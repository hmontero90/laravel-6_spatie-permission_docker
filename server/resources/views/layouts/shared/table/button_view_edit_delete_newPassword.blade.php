<button class="btn dropdown-toggle"
        type="button"
        id="dropdownMenuButton"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false">
    <i class="fas fa-fw fa-cog"></i>
</button>
<div class="dropdown-menu animated--fade-in"
     aria-labelledby="dropdownMenuButton"
     style="">

    <!-- Botón ver -->
    @include('layouts.shared.table.buttons.view')
    <!-- Fin Botón ver -->

    <!-- Botón editar -->
    @include('layouts.shared.table.buttons.edit')
    <!-- Fin Botón editar -->

    <!-- Botón eliminar -->
    @include('layouts.shared.table.buttons.delete')
    <!-- Fin Botón eliminar -->

    <!-- Botón cambiar contraseña -->
    @include('layouts.shared.table.buttons.newPassword')
    <!-- Fin Botón cambiar contraseña -->

</div>



