<a class="nav-link dropdown-toggle"
   href="#"
   id="userDropdown"
   role="button"
   data-toggle="dropdown"
   aria-haspopup="true"
   aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                    @auth()
                        {{ Auth::user()->name }}
                    @endauth
                </span>
    @auth()
        @if(isset(Auth::user()->avatar))
            <img class="img-profile rounded-circle"
                 src="/usuarios/perfil/avatar">
        @else
            <img class="img-profile rounded-circle"
                 src="/img/photo.jpg">
        @endif
    @endauth
</a>
