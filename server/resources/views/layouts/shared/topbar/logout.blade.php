<a class="dropdown-item"
   href="#"
   data-toggle="modal"
   data-target="#logoutModal">
    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
    Cerrar sesión
</a>
