<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop"
            class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    <!-- Search -->
    {{--@include('layouts.shared.topbar.search')--}}
    <!-- FinSearch -->

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
{{--        @include('layouts.shared.topbar.search_dropdown')--}}
        <!-- Fin Nav Item - Search Dropdown (Visible Only XS) -->

        <!-- Alertas -->
{{--        @include('layouts.shared.topbar.alert')--}}
        <!-- Fin Alertas -->

        <!-- Mensajes -->
{{--        @include('layouts.shared.topbar.messages')--}}
        <!-- Mensajes -->

        @auth()
        <div class="topbar-divider d-none d-sm-block"></div>

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <!-- Usuario -->
            @include('layouts.shared.topbar.user')
            <!-- Fin Usuario-->

            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                 aria-labelledby="userDropdown">

                <!-- Perfil -->
                @include('layouts.shared.topbar.profile')
                <!-- Fin Perfil -->

                <!-- Cambiar Contraseña -->
                @include('layouts.shared.topbar.changePassword')
                <!-- Fin Cambiar Contraseña -->

                <div class="dropdown-divider"></div>

                <!-- Cerrar seccion -->
                @include('layouts.shared.topbar.logout')
                <!-- Cerrar seccion -->
            </div>
        </li>
        @endauth

    </ul>

</nav>
