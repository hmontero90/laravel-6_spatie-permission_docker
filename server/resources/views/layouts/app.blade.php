<!DOCTYPE html>
<html lang="en">

@include('layouts.shared.head')

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    @include('layouts.shared.sidebar')
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            @include('layouts.shared.topbar')
            <!-- Fin Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

                @yield('content')

            </div>
            <!-- Fin Begin Page Content -->

        </div>
        <!-- Fin Main Content -->

        <!-- Footer -->
        @include('layouts.shared.footer')
        <!-- Fin Footer -->

    </div>
    <!-- Fin Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
@include('layouts.shared.scroll_to_top_button')
<!-- Fin Scroll to Top Button-->

<!-- Logout Modal-->
@include('layouts.shared.logout_modal')
<!-- Fin Logout Modal-->

<!-- Modal de confirmacion-->
@include('layouts.shared.dataConfirmModel')
<!-- Modal de confirmacion-->

<!-- Scrits -->
@include('layouts.shared.script')
@stack('scripts')
@yield('scripts')
<!-- Fin Scrits -->

</body>

</html>
