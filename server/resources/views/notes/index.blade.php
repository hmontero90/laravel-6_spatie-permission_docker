@extends('layouts.app')

@section('title', 'Listado de notas')

@section('content')

<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-sm display">
            <thead class="thead-dark">
                <th class="text-center">ID</th>
                <th class="text-center">Título</th>
                <th class="text-center">Acción</th>
            </thead>
            <tbody>
            @foreach ($notes as $note)
                <tr>
                    <td>{{ $note->id }}</td>
                    <td>{{ $note->title }}</td>
                    <td>
                        @include('layouts.tabla-acciones', [
                            'permiso_ver' => null,
                            'permiso_editar' => null,
                            'permiso_eliminar' => 'destroy_notes',
                            'url_ver' => null,
                            'url_editar' => null,
                            'url_eliminar' => route('notes.destroy', $note->id)
                        ])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection
