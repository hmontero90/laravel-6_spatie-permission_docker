<div class="form-group row">
    <label for="title"
           class="col-md-4 col-form-label text-md-right">
        Título:
    </label>

    <div class="col-md-6">
        <input id="title"
               type="text"
               class="form-control @error('title') is-invalid @enderror"
               name="title"
               title="Título"
               @if(isset($note))
               value="{{ $note->title }}"
               @else
               value="{{ old('title') }}"
               @endif
               required
               autocomplete="title"
               autofocus>

        @error('title')
        <span class="invalid-feedback"
              role="alert">
            <strong>
                {{ $message }}
            </strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="content"
           class="col-md-4 col-form-label text-md-right">
        Contenido:
    </label>

    <div class="col-md-6">
        <textarea id="content"
               class="form-control @error('content') is-invalid @enderror"
               name="content"
               title="Contenido"
               required
               autocomplete="note_content"
               autofocus>{{ isset($note->content) ? $note->content : '' }}</textarea>

        @error('content')
        <span class="invalid-feedback"
              role="alert">
            <strong>
                {{ $message }}
            </strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
        @include('layouts.shared.button.cancel', [
            'route' => '/notas'
        ])
        @include('layouts.shared.button.submit')
    </div>
</div>

