@extends('layouts.app')

@section('title', 'Crear Notas')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-12">

                <!-- Div Titulo -->
                <div class="card-header py-3">
                    <div class="float-left">
                        <h5 class="m-0 font-weight-bold text-primary">
                            Crear Notas
                        </h5>
                    </div>
                    <div class="float-right">
                    </div>
                </div>
                <!-- Fin Div Titulo -->

                <!-- Div Contenido -->
                <div class="card-body">

                    <!-- Formulario -->
                    <form action="/notas/crear" method="POST">
                        @csrf

                        <!-- Vista del formulario -->
                        @include('notes.shared.form')
                        <!-- Fin Vista del formulario -->

                    </form>
                </div>
                <!-- Fin Div Contenido -->

            </div>
        </div>
    </div>
@endsection
