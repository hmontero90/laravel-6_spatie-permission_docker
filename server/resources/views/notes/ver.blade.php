@extends('layouts.app')

@section('title', 'Ver notas')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-12">

                <!-- Div Titulo -->
                <div class="card-header py-3">
                    <div class="float-left">
                        <h5 class="m-0 font-weight-bold text-primary">
                            Ver notas
                        </h5>
                    </div>
                    <div class="float-right">
                        <!-- -->
                    </div>
                </div>
                <!-- Fin Div Titulo -->

                <!-- Div Contenido -->
                <div class="card-body">
                    <div class="form-group row">
                        <label for="name"
                               class="col-md-4 col-form-label text-md-right">
                            Título:
                        </label>
                        <div class="col-md-6">
                            <input id="title"
                                   type="text"
                                   class="form-control"
                                   name="title"
                                   title="Título"
                                   value="{{ $note->title }}"
                                   autofocus
                                   disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="roles"
                               class="col-md-4 col-form-label text-md-right">
                            Contenido:
                        </label>
                        <div class="col-md-6">
                        <textarea id="content"
                                  class="form-control"
                                  name="content"
                                  title="Contenido"
                                  autofocus
                                  disabled>{{ $note->content }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            @include('layouts.shared.button.cancel', [
                                'route' => '/notas'
                            ])
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
