@extends('layouts.app')

@section('title', 'Ver Roles')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-12">

                <!-- Div Titulo -->
                <div class="card-header py-3">
                    <div class="float-left">
                        <h5 class="m-0 font-weight-bold text-primary">
                            Ver rol
                        </h5>
                    </div>
                    <div class="float-right">
                        <!-- -->
                    </div>
                </div>
                <!-- Fin Div Titulo -->

                <!-- Div Contenido -->
                <div class="card-body">
                    <div class="form-group row">
                        <label for="name"
                               class="col-md-4 col-form-label text-md-right">
                            Nombre:
                        </label>

                        <div class="col-md-6">
                            <input id="name"
                                   type="text"
                                   class="form-control"
                                   name="name"
                                   title="Nombre"
                                   @if(isset($role))
                                   value="{{ $role->display_name }}"
                                   @endif
                                   disabled>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="roles"
                               class="col-md-4 col-form-label text-md-right">
                            Permisos:
                        </label>
                        <div class="col-md-6">
                            @if(isset($permissions) && isset($permissionsIsChecked))
                                @foreach($permissions as $permission)
                                    @if(in_array($permission->id, $permissionsIsChecked, true))
                                        <button type="button"
                                                class="btn btn-success btn-icon-split col-xs-2 button-separation"
                                                role="group">
                                            <span class="icon text-white-50">
                                                <input type="checkbox"
                                                       checked
                                                       name="permissions[]"
                                                       title="{{$permission->display_name}}"
                                                       value="{{$permission->name}}"
                                                       disabled>
                                            </span>
                                            <span class="text">
                                    {{$permission->display_name}}
                                </span>
                                        </button>
                                    @else
                                        <button type="button"
                                                class="btn btn-success btn-icon-split col-xs-2 button-separation"
                                                role="group">
                                            <span class="icon text-white-50">
                                                <input type="checkbox"
                                                       name="permissions[]"
                                                       title="{{$permission->display_name}}"
                                                       value="{{$permission->name}}"
                                                       disabled>
                                            </span>
                                            <span class="text">
                                                {{$permission->display_name}}
                                            </span>
                                        </button>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            @include('layouts.shared.button.cancel', [
                                'route' => '/roles'
                            ])
                        </div>
                    </div>

                </div>
                <!-- Fin Div Contenido -->

            </div>
        </div>
    </div>
@endsection
