@extends('layouts.app')

@section('title', 'Listado de Roles')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-12">

                <!-- Div Titulo -->
                <div class="card-header py-3">
                    <div class="float-left">
                        <h5 class="m-0 font-weight-bold text-primary">
                            Listado de los Roles
                        </h5>
                    </div>
                    <div class="float-right">
                        @include('layouts.shared.button.add', [
                                'permiso_agregar' => 'create_role',
                                'route' => '/roles/crear'
                            ])
                    </div>
                </div>
                <!-- Fin Div Titulo -->

                <!-- Div Contenido -->
                <div class="card-body">

                    <!-- Listado de los usuarios -->
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-sm display"
                               style="width:100%"
                               id="roles-table">
                            <thead class="thead-dark">
                            <th class="text-center" style="width:10px;">ID</th>
                            <th class="text-center">Nombre</th>
                            <th class="text-center" style="width:100px;">Permisos</th>
                            <th class="text-center" style="width:200px;">Fecha de creación</th>
                            <th class="text-center" style="width:200px;">Fecha de actualización</th>
                            <th class="text-center" style="width:10px;">Acciones</th>
                            </thead>
                        </table>
                    </div>
                    <!-- Fin Listado de los usuarios -->

                </div>
                <!-- Fin Div Contenido -->

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(function() {
            $('#roles-table').DataTable({
                ajax: '/roles/datos',
                columns: [
                    {data: 'id', name: 'id', visible: true},
                    {data: 'display_name', name: 'display_name'},
                    {data: 'permissions', name: 'permissions', orderable:false},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions', orderable:false}
                ]
            });
        });
    </script>
@endsection
