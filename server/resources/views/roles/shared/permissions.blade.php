<a class="dropdown-item"
   href="#"
   data-toggle="modal"
   data-target="#roleModal{{$role->id}}">
    <i class="fas fa-eye fa-sm fa-fw mr-2 text-gray-400"></i>
    Ver Permisos
</a>

<div class="modal fade"
     id="roleModal{{$role->id}}"
     tabindex="-1"
     role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog"
         role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"
                    id="exampleModalLabel">
                    Permisos del Role: {{$role->display_name}}
                </h5>
                <button class="close"
                        type="button"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                @if(isset($permissions))
                    @foreach($permissions as $permission)
                        <button type="button"
                                class="btn btn-outline-primary button-separation">
                            {{$permission}}
                        </button>
                    @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary"
                        type="button"
                        data-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
