<div class="form-group row">
    <label for="display_name"
           class="col-md-4 col-form-label text-md-right">
        Nombre:
    </label>

    <div class="col-md-6">
        <input id="display_name"
               type="text"
               class="form-control @error('display_name') is-invalid @enderror"
               name="display_name"
               title="Nombre"
               @if(isset($role))
               value="{{ $role->display_name }}"
               @else
               value="{{ old('display_name') }}"
               @endif
               required
               autocomplete="name"
               autofocus>

        @error('display_name')
        <span class="invalid-feedback"
              role="alert">
            <strong>
                {{ $message }}
            </strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="roles"
           class="col-md-4 col-form-label text-md-right">
        Permisos:
    </label>
    <div class="col-md-6">
        @if(isset($permissions))
            @if(isset($role))
                @foreach($permissions as $permission)
                    @if(in_array($permission->id, $permissionsIsChecked, true))
                        <button type="button"
                                class="btn btn-success btn-icon-split col-xs-2 button-separation"
                                role="group">
                        <span class="icon text-white-50">
                            <input type="checkbox"
                                   checked
                                   name="permissions[]"
                                   value="{{$permission->name}}">
                        </span>
                            <span class="text">
                            {{$permission->display_name}}
                        </span>
                        </button>
                    @else
                        <button type="button"
                                class="btn btn-success btn-icon-split col-xs-2 button-separation"
                                role="group">
                            <span class="icon text-white-50">
                                <input type="checkbox"
                                       name="permissions[]"
                                       value="{{$permission->name}}">
                            </span>
                            <span class="text">
                                {{$permission->display_name}}
                            </span>
                        </button>
                    @endif
                @endforeach

            @else
                <div class="form-group">
                    @foreach($permissions as $permission)
                        <button type="button"
                                class="btn btn-success btn-icon-split col-xs-2 button-separation"
                                role="group">
                        <span class="icon text-white-50">
                            <input type="checkbox"
                                   name="permissions[]"
                                   value="{{$permission->name}}">
                        </span>
                            <span class="text">
                            {{$permission->display_name}}
                        </span>
                        </button>
                    @endforeach
                </div>
            @endif
        @endif
    </div>
</div>

<div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
        @include('layouts.shared.button.cancel', [
            'route' => '/roles'
        ])
        @include('layouts.shared.button.submit')
    </div>
</div>

