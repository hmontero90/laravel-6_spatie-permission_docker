<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class Note extends Model
{
    use HasRoles;
    use SoftDeletes;

    /**
     * Nombre de la tabla en base de datos
     *
     * @var string
     */
    protected $table = 'notes';

    /**
     * Campos rellenable
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content',
        'created_by_user_id',
        'updated_by_user_id',
    ];

    /**
     * SoftDeletes
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];
}
