<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Permission extends Model
{
    use HasRoles;

    /**
     * Nombre de la tabla en base de datos
     *
     * @var string
     */
    protected $table = 'permissions';

    /**
     * Campos rellenable
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'guard_name',
        'display_name'
    ];
}
