<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class Role extends Model
{
    use HasRoles;
    use SoftDeletes;

    /**
     * Nombre de la tabla en base de datos
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * Campos rellenable
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'guard_name',
        'display_name',
        'created_by_user_id',
        'updated_by_user_id',
    ];

    /**
     * Niveles del role
     *
     * @var array
     */
    protected $hidden = [
        'level',
    ];

    /**
     * SoftDeletes
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roleHasPermissions()
    {
        return $this->hasMany(RoleHasPermissions::class, 'role_id' ,'id');
    }
}
