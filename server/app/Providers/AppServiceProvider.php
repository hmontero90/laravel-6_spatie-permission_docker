<?php

namespace App\Providers;

use App\Rules\CustomValidation;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::resolver(function($translator, $data, $rules, $messages, $attibute)
        {
            return new CustomValidation($translator, $data, $rules, $messages, $attibute);
        });
    }
}
