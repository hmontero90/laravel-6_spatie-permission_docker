<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|min:1|max:255',
        ];

        // Id del usuario iniciado
        $authUserEmail = auth()->user()->email;

        // Si el correo electrónico es distinto al del usuario iniciado
        // validamos el correo electrónico
        if (isset($this->email)
            && $this->email != $authUserEmail){
            $rules['email'] = 'required|string|email|min:5|max:255|unique:users,email';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'name'  => 'Nombre',
            'email' => 'Correo electrónico'
        ];
    }

    public function messages()
    {
        return [
            // Nombre
            'name.required' => 'El :attribute es requerido.',
            'name.string'   => 'El :attribute tiene que ser un string.',
            'name.min'      => 'El :attribute tiene que tener mas de :min caracteres.',
            'name.max'      => 'El :attribute tiene que tener menos de :max caracteres.',

            // Correo electrónico
            'email.unique'   => 'El :attribute ya existe en el sistema.',
            'email.required' => 'El :attribute es requerido.',
            'email.string'   => 'El :attribute tiene que ser un string.',
            'email.email'    => 'El: attribute debe ser una dirección de correo electrónico válida.',
            'email.min'      => 'El :attribute tiene que tener mas de :min caracteres.',
            'email.max'      => 'El :attribute tiene que tener menos de :max caracteres.',
        ];
    }

}
