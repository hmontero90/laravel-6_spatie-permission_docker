<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'password' => 'required|string|min:8|max:255|confirmed'
        ];

        return $rules;
    }

    public function attributes()
    {
        return [
            'password' => 'Contraseña'
        ];
    }

    public function messages()
    {
        return [
            'password.required' => 'La :attribute es requerida.',
            'password.string'   => 'La :attribute debe ser un string.',
            'password.min'      => 'La :attribute debe tener como mínimo :min caracteres.',
            'password.max'      => 'La :attribute debe tener como máximo :max caracteres.',
            'password.confirmed'=> 'Se necesita confirmar :attribute.',
        ];
    }

}
