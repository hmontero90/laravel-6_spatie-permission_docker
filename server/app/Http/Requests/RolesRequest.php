<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class RolesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'display_name' => 'required|string|min:1|max:255|unique_not_deleted:roles,display_name,'.$this->display_name,
        ];

        $nameInDb = DB::table('roles')
            ->where('id', $this->id)
            ->pluck('display_name')
            ->first();

        /* Actualizacion */
        if ($this->id && $nameInDb == $this->display_name) {
            $rules['display_name'] .= ',' . $this->id;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'display_name.unique_not_deleted' => 'El :attribute ya existe en la base de datos'
        ];
    }

    public function attributes()
    {
        return [
            'display_name' => 'Nombre'
        ];
    }
}
