<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class NotesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|string|min:1|max:255|unique_not_deleted:notes,title,'.$this->title,
        ];

        $nameInDb = DB::table('notes')
            ->where('id', $this->id)
            ->pluck('title')
            ->first();

        /* Actualizacion */
        if ($this->id && $nameInDb == $this->title) {
            $rules['title'] .= ',' . $this->id;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'title.unique_not_deleted' => 'El :attribute ya existe en la base de datos'
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Título'
        ];
    }
}
