<?php

namespace App\Http\Controllers;

use App\Http\Requests\RolesRequest;
use App\RoleRepository;
use App\UserRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Spatie\Permission\Models\Role as SpatieRole;
use App\Role;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str as Str;

class RolesController extends Controller
{
    /**
     * Vista del listado
     *
     * @return Factory|View
     */
    public function lists()
    {
        return view('roles.listado');
    }

    /**
     * Datos para el listado
     *
     * @return mixed
     * @throws \Exception
     */
    public function getListsData(RoleRepository $roleRepository)
    {
        // Trae todos los roles
        $roles = Role::with('roleHasPermissions', 'roleHasPermissions.permissions')
            ->select(['id','display_name','created_at', 'updated_at']);

        // Trae todos los roles segun los roles que tenga el usuario
        $roles = $roleRepository->rolesPermissionsCase($roles);

        return Datatables::of($roles)
            ->addColumn('actions', function ($row) {
                $permiso_ver = 'view_role';
                $permiso_editar = 'edit_role';
                $permiso_eliminar = 'delete_role';
                $url_ver = '/roles/ver/'.$row->id;
                $url_editar = '/roles/editar/'.$row->id;
                $url_eliminar = 'roles/eliminar/'.$row->id;

                return view(
                    'layouts.shared.table.button_view_edit_delete',
                    compact(
                        'permiso_ver',
                        'permiso_editar',
                        'permiso_eliminar',
                        'url_ver',
                        'url_editar',
                        'url_eliminar'
                    )
                );
            })
            ->addColumn('permissions', function ($row) {
                $role = $row;
                $permissions = $row->roleHasPermissions->pluck('permissions.display_name');

                return view(
                    'roles.shared.permissions',
                    compact(
                        'role',
                        'permissions'
                    )
                );
            })
            ->make([
                'permissions'
            ]);
    }

    /**
     * @param RoleRepository $roleRepository
     * @return Factory|View
     */
    public function create(RoleRepository $roleRepository)
    {
        // Permisos del usuario iniciado
        $permissions = $roleRepository->userPermissionsCase();

        return view('roles.crear',
            compact(
                'permissions'
            )
        );
    }

    /**
     * @param RolesRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(RolesRequest $request)
    {
        // Id Usuario iniciado
        $userRepository = new UserRepository();
        $authUserId = $userRepository->authUser('id');

        // Datos
        $data = $request->input();

        // Permisos
        $permissions = $request->input('permissions');

        // Crea el nuevo role
        $role = SpatieRole::create([
            'name' => Str::slug($data['display_name']),
            'display_name' => $data['display_name'],
            'level' => 3,
            'created_by_user_id' => $authUserId,
            'updated_by_user_id' => $authUserId,
        ]);

        // Agrega los permisos al nuevo role
        $role->givePermissionTo($permissions);

        return redirect('/roles');
    }

    /**
     * Vista de Ver usuario
     *
     * @param $id
     * @param RoleRepository $roleRepository
     * @return Factory|View
     */
    public function show($id, RoleRepository $roleRepository)
    {
        // Usuario
        $role = Role::with('roleHasPermissions.permissions')
            ->findOrFail($id);

        // Permisos del usuario iniciado
        $permissions = $roleRepository->userPermissionsCase();

        $permissionsIsChecked = $role->roleHasPermissions
            ->pluck('permission_id')
            ->toArray();

        return view('roles.ver',
            compact(
                'role',
                'permissions',
                'permissionsIsChecked'
            ));
    }

    /**
     * Vista del editar
     *
     * @param $id
     * @param RoleRepository $roleRepository
     * @return Factory|View
     */
    public function edit($id, RoleRepository $roleRepository)
    {
        $role = Role::with('roleHasPermissions.permissions')
            ->findOrFail($id);

        // Permisos del usuario iniciado
        $permissions = $roleRepository->userPermissionsCase();

            $permissionsIsChecked = $role->roleHasPermissions
            ->pluck('permission_id')
            ->toArray();

        return view('roles.editar',
            compact(
                'role',
                'permissions',
                'permissionsIsChecked'
            )
        );
    }

    /**
     * Envia los datos de la edicion
     *
     * @param $id
     * @param RolesRequest $request
     * @param UserRepository $userRepository
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, RolesRequest $request, UserRepository $userRepository)
    {
        // Id del usuario iniciado
        $userRepository = new UserRepository();
        $authUserId = $userRepository->authUser('id');

        // Datos
        $data = $request->input();
        $data['updated_by_user_id'] = $authUserId;

        // Permisos
        $permissions = $request->input('permissions');

        // Crea el nuevo role
        $role = Role::findOrFail($id);
        $role->update($data);

        // Agrega los permisos al nuevo role
        $spatieRole = SpatieRole::findById($id);
        $spatieRole->givePermissionTo($permissions);

        return redirect('/roles');
    }

    /**
     * Elimina
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // Id del usuario iniciado
        $userRepository = new UserRepository();
        $authUserId = $userRepository->authUser('id');

        // Elimina el usuario
        $role = Role::findOrFail($id);
        $role->update([
            'updated_by_user_id' => $authUserId
        ]);
        $role->delete();

        return redirect('/roles');
    }

    /**
     * Recupera el usuario eliminado
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function restore($id)
    {
        // Id del usuario iniciado
        $userRepository = new UserRepository();
        $authUserId = $userRepository->authUser('id');

        // Recuperar el usuario
        $role = Role::withTrashed()
            ->findOrFail($id);
        $role->update([
            'updated_by_user_id' => $authUserId
        ]);
        $role->restore();

        return redirect('/roles');
    }

    /**
     * Vista del listado de la papelera
     *
     * @return Factory|View
     */
    public function listsTrash()
    {
        return view('roles.papelera');
    }

    /**
     * Datos para el listado de la papelera
     *
     * @param RoleRepository $roleRepository
     * @return mixed
     * @throws \Exception
     */
    public function getListsTrashData(RoleRepository $roleRepository)
    {
        // Trae todos los roles
        $roles = Role::onlyTrashed()
            ->with('roleHasPermissions', 'roleHasPermissions.permissions')
            ->select(['id','display_name','created_at', 'updated_at']);

        // Trae todos los roles segun los roles que tenga el usuario
        $roles = $roleRepository->rolesPermissionsCase($roles);

        return Datatables::of($roles)
            ->addColumn('actions', function ($row) {
                $permiso_ver = 'view_role';
                $url_ver = '/roles/ver/'.$row->id;
                $permiso_restaurar = 'restore_role';
                $url_restaurar = '/roles/restaurar/'.$row->id;

                return view(
                    'layouts.shared.table.button_view_restore',
                    compact(
                        'permiso_ver',
                        'url_ver',
                        'permiso_restaurar',
                        'url_restaurar'
                    )
                );
            })
            ->addColumn('permissions', function ($row) {
                $role = $row;
                $permissions = $row->roleHasPermissions->pluck('permissions.display_name');

                return view(
                    'roles.shared.permissions',
                    compact(
                        'role',
                        'permissions'
                    )
                );
            })
            ->make([
                'permissions'
            ]);
    }

}
