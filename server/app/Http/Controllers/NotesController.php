<?php
namespace App\Http\Controllers;

use App\Http\Requests\NotesRequest;
use App\RoleRepository;
use App\UserRepository;
use Illuminate\Contracts\View\Factory;
use App\Note;
use Illuminate\View\View;
use Yajra\DataTables\Facades\DataTables;

class NotesController extends Controller
{
    /**
     * Vista del listado
     *
     * @return Factory|View
     */
    public function lists()
    {
        return view('notes.listado');
    }

    /**
     * Datos para el listado
     *
     * @return mixed
     * @throws \Exception
     */
    public function getListsData()
    {
        // Trae todos los roles
        $notes = Note::select(['id','title', 'content']);

        return Datatables::of($notes)
            ->addColumn('actions', function ($row) {
                $permiso_ver = 'view_note';
                $permiso_editar = 'edit_note';
                $permiso_eliminar = 'delete_note';
                $url_ver = '/notas/ver/'.$row->id;
                $url_editar = '/notas/editar/'.$row->id;
                $url_eliminar = 'notas/eliminar/'.$row->id;

                return view(
                    'layouts.shared.table.button_view_edit_delete',
                    compact(
                        'permiso_ver',
                        'permiso_editar',
                        'permiso_eliminar',
                        'url_ver',
                        'url_editar',
                        'url_eliminar'
                    )
                );
            })
            ->make('actions')
            ;
    }

    /**
     * @param RoleRepository $roleRepository
     * @return Factory|View
     */
    public function create(RoleRepository $roleRepository)
    {
        return view('notes.crear');
    }

    /**
     * @param NotesRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(NotesRequest $request)
    {
        // Id del usuario iniciado
        $userRepository = new UserRepository();
        $authUserId = $userRepository->authUser('id');

        // Datos
        $data = $request->input();
        $data['created_by_user_id'] = $authUserId;
        $data['updated_by_user_id'] = $authUserId;

        // Crea la nota
        $note = new Note();
        $note->fill($data);
        $note->save();

        return redirect('/notas');
    }

    /**
     * Vista de Ver usuario
     *
     * @param $id
     * @return Factory|View
     */
    public function show($id)
    {
        // Ver las notas
        $note = Note::findOrFail($id);

        return view('notes.ver',
            compact(
                'note'
            ));
    }

    /**
     * Vista del editar
     *
     * @param $id
     * @param RoleRepository $roleRepository
     * @return Factory|View
     */
    public function edit($id, RoleRepository $roleRepository)
    {
        $note = Note::findOrFail($id);

        return view('notes.editar',
            compact(
                'note'
            )
        );
    }

    /**
     * Envia los datos de la edicion
     *
     * @param $id
     * @param NotesRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, NotesRequest $request)
    {
        // Id del usuario iniciado
        $userRepository = new UserRepository();
        $authUserId = $userRepository->authUser('id');

        // Datos
        $data = $request->input();
        $data['updated_by_user_id'] = $authUserId;

        // Crea el nuevo role
        $note = Note::findOrFail($id);
        $note->update($data);

        return redirect('/notas');
    }

    /**
     * Elimina
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // Id del usuario iniciado
        $userRepository = new UserRepository();
        $authUserId = $userRepository->authUser('id');

        // Elimina el usuario
        $note = Note::findOrFail($id);
        $note->update([
            'updated_by_user_id' => $authUserId
        ]);
        $note->delete();

        return redirect('/notas');
    }

    /**
     * Recupera el usuario eliminado
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function restore($id)
    {
        // Id del usuario iniciado
        $userRepository = new UserRepository();
        $authUserId = $userRepository->authUser('id');

        // Recuperar el usuario
        $note = Note::withTrashed()
            ->findOrFail($id);
        $note->update([
            'updated_by_user_id' => $authUserId
        ]);
        $note->restore();

        return redirect('/notas/papelera');
    }

    /**
     * Vista del listado de la papelera
     *
     * @return Factory|View
     */
    public function listsTrash()
    {
        return view('notes.papelera');
    }

    /**
     * Datos para el listado de la papelera
     *
     * @return mixed
     * @throws \Exception
     */
    public function getListsTrashData()
    {
        // Trae todos los roles
        $note = Note::onlyTrashed()
            ->select(['id','title','content']);

        return Datatables::of($note)
            ->addColumn('actions', function ($row) {
                $permiso_ver = 'view_note';
                $url_ver = '/notas/ver/'.$row->id;
                $permiso_restaurar = 'restore_note';
                $url_restaurar = '/notas/restaurar/'.$row->id;

                return view(
                    'layouts.shared.table.button_view_restore',
                    compact(
                        'permiso_ver',
                        'url_ver',
                        'permiso_restaurar',
                        'url_restaurar'
                    )
                );
            })
            ->make([
                'actions'
            ]);
    }

}
