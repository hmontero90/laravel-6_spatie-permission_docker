<?php
namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Http\Requests\UsersChangePasswordRequest;
use App\RoleRepository;
use App\User;
use App\Http\Requests\UsersRequest;
use App\UserRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;
use Yajra\DataTables\Facades\DataTables;
use Intervention\Image\Facades\Image;

class UsersController extends Controller
{
    /**
     * Vista del listado
     *
     * @return Factory|View
     */
    public function lists()
    {
        return view('usuarios.listado');
    }

    /**
     * Datos para el listado
     *
     * @param UserRepository $userRepository
     * @return mixed
     * @throws \Exception
     */
    public function getListsData(UserRepository $userRepository)
    {
        // Usuario logeado
        $authUserEmail = $userRepository->authUser('email');

        // Trae todos los usuarios con sus roles
        $users = User::with('roles')
            ->select(['id','name','email']);

        // Muestra los usuarios con sus roles segun corresponda
        $users = $userRepository->userRoleCase($users);

        return Datatables::of($users)
            ->addColumn('actions', function ($row) use ($authUserEmail) {
                switch ($authUserEmail) {
                    case $authUserEmail === $row->email:
                        $permiso_ver = 'view_user';
                        $permiso_editar = 'edit_user';
                        $permiso_eliminar = null;
                        $permiso_cambiarContrasenia = 'changePassword_user';
                        $url_ver = '/usuarios/ver/'.$row->id;
                        $url_editar = '/usuarios/editar/'.$row->id;
                        $url_eliminar = null;
                        $url_cambiarContrasenia = '/usuarios/cambiar/contrasenia/'.$row->id;
                        break;
                    default:
                        $permiso_ver = 'view_user';
                        $permiso_editar = 'edit_user';
                        $permiso_eliminar = 'delete_user';
                        $permiso_cambiarContrasenia = 'changePassword_user';
                        $url_ver = '/usuarios/ver/'.$row->id;
                        $url_editar = '/usuarios/editar/'.$row->id;
                        $url_eliminar = 'usuarios/eliminar/'.$row->id;
                        $url_cambiarContrasenia = '/usuarios/cambiar/contrasenia/'.$row->id;
                        break;
                }
                return view(
                    'layouts.shared.table.button_view_edit_delete_newPassword',
                    compact(
                        'permiso_ver',
                        'permiso_editar',
                        'permiso_eliminar',
                        'permiso_cambiarContrasenia',
                        'url_ver',
                        'url_editar',
                        'url_eliminar',
                        'url_cambiarContrasenia'
                    )
                );
            })
            ->addColumn('roles', function ($row) {
                $item = isset($row->roles->first()->display_name)
                    ? $row->roles->first()->display_name
                    : 'No se encontro el role'
                ;
                return $item;
            })
            ->make();
    }

    /**
     * Vista del Crear
     *
     * @return Factory|View
     */
    public function create()
    {
        // Roles
        $roleRepository = new RoleRepository();
        $roles = $roleRepository->roleAccordingToUser();

        return view('usuarios.crear',
            compact(
                'roles'
            )
        );
    }

    /**
     * Envia los datos de la creacion
     *
     * @param UsersRequest $request
     * @param UserRepository $userRepository
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(UsersRequest $request, UserRepository $userRepository)
    {
        // Id del usuario iniciado
        $authUserId = $userRepository->authUser('id');

        // Datos
        $data = $request->input();
        $data['password'] = Hash::make($request->input('password'));
        $data['created_by_user_id'] = $authUserId;
        $data['updated_by_user_id'] = $authUserId;

        // Permisos
        $role = $request->input('roles');

        // Crea el nuevo role
        $user = new User;
        $user->fill($data);
        $user->save();

        // Agrega el role al nuevo usuario
        $user->assignRole($role);

        return redirect('/usuarios');
    }

    /**
     * Vista de Ver usuario
     *
     * @param $id
     * @return Factory|View
     */
    public function show($id)
    {
        // Usuario
        $user = User::with('roles')
            ->findOrFail($id);

        return view('usuarios.ver',
            compact(
                'user'
            ));
    }

    /**
     * Vista del editar
     *
     * @param $id
     * @param RoleRepository $roleRepository
     * @return Factory|View
     */
    public function edit($id)
    {
        // Roles
        $roleRepository = new RoleRepository();
        $roles = $roleRepository->roleAccordingToUser();

        // Usuario
        $user = User::with('roles')
            ->findOrFail($id);

        return view('usuarios.editar',
            compact(
                'roles',
                'user'
            )
        );
    }

    /**
     * Envia los datos de la edicion
     *
     * @param $id
     * @param UsersRequest $request
     * @param UserRepository $userRepository
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, UsersRequest $request, UserRepository $userRepository)
    {
        // Id del usuario iniciado
        $authUserId = $userRepository->authUser('id');

        // Datos
        $data = $request->input();
        $data['updated_by_user_id'] = $authUserId;

        // Permisos
        $role = $request->input('roles');

        // Crea el nuevo role
        $user = User::findOrFail($id);
        $user->update($data);

        // Agrega el role al nuevo usuario
        $user->syncRoles($role);

        return redirect('/usuarios');
    }

    /**
     * Elimina
     *
     * @param $id
     * @param UserRepository $userRepository
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, UserRepository $userRepository)
    {
        // Id del usuario iniciado
        $authUserId = $userRepository->authUser('id');

        // Elimina el usuario
        $user = User::findOrFail($id);
        $user->update([
            'updated_by_user_id' => $authUserId
        ]);
        $user->delete();

        return redirect('/usuarios');
    }

    /**
     * Recupera el usuario eliminado
     *
     * @param $id
     * @param UserRepository $userRepository
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function restore($id, UserRepository $userRepository)
    {
        // Id del usuario iniciado
        $authUserId = $userRepository->authUser('id');

        // Recuperar el usuario
        $user = User::withTrashed()
            ->findOrFail($id);
        $user->update([
            'updated_by_user_id' => $authUserId
        ]);
        $user->restore();

        return redirect('/usuarios');
    }

    /**
     * Vista del listado de la papelera
     *
     * @return Factory|View
     */
    public function listsTrash()
    {
        return view('usuarios.papelera');
    }

    /**
     * Datos para el listado de la papelera
     *
     * @param UserRepository $userRepository
     * @return mixed
     * @throws \Exception
     */
    public function getListsTrashData(UserRepository $userRepository)
    {
        // Trae todos los usuarios con sus roles
        $users = User::onlyTrashed()
            ->with('roles')
            ->select(['id','name','email']);

        // Muestra los usuarios con sus roles segun corresponda
        $users = $userRepository->userRoleCase($users);

        return Datatables::of($users)
            ->addColumn('actions', function ($row) {
                $permiso_ver = 'view_user';
                $url_ver = '/usuarios/ver/'.$row->id;
                $permiso_restaurar = 'restore_user';
                $url_restaurar = '/usuarios/restaurar/'.$row->id;

                return view(
                    'layouts.shared.table.button_view_restore',
                    compact(
                        'permiso_ver',
                        'url_ver',
                        'permiso_restaurar',
                        'url_restaurar'
                    )
                );
            })
            ->addColumn('roles', function ($row) {
                $item = isset($row->roles->first()->display_name)
                    ? $row->roles->first()->display_name
                    : 'No se encontro el role'
                ;
                return $item;
            })
            ->make();
    }

    /**
     * Vista del usuario para cambiar contraseña
     *
     * @param $id
     * @return Factory|View
     */
    public function changePassword($id)
    {
        // Usuario
        $user = User::findOrFail($id);

        return view('usuarios.cambiarContrasenia',
            compact(
                'user'
            )
        );
    }

    /**
     * Envia los datos para cambiar la contraseña del usuario
     *
     * @param $id
     * @param UsersChangePasswordRequest $request
     * @param UserRepository $userRepository
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function newPassword($id, UsersChangePasswordRequest $request, UserRepository $userRepository)
    {
        // Id del usuario iniciado
        $authUserId = $userRepository->authUser('id');

        // Datos
        $data = $request->input();
        $data['updated_by_user_id'] = $authUserId;

        // Crea el nuevo role
        $user = User::findOrFail($id);
        $user->update($data);

        return redirect('/usuarios');
    }

    /**
     * Vista del usuario para cambiar contraseña
     *
     * @param UserRepository $userRepository
     * @return Factory|View
     */
    public function changePasswordUserAuth(UserRepository $userRepository)
    {
        // Id del usuario iniciado
        $authUserId = $userRepository->authUser('id');

        // Usuario
        $user = User::findOrFail($authUserId);

        return view('usuarios.cambiarContraseniaUsuarioIniciado',
            compact(
                'user'
            )
        );
    }

    /**
     * Envia los datos para cambiar la contraseña del usuario
     *
     * @param UsersChangePasswordRequest $request
     * @param UserRepository $userRepository
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function newPasswordUserAuth(UsersChangePasswordRequest $request, UserRepository $userRepository)
    {
        // Id del usuario iniciado
        $authUserId = $userRepository->authUser('id');

        // Datos
        $data = $request->input();

        // Crea el nuevo role
        $user = User::findOrFail($authUserId);
        $user->update($data);

        return redirect('/home');
    }

    /**
     * Vista del perfil del usuario
     *
     * @param UserRepository $userRepository
     * @return Factory|View
     */
    public function editUserAuth(UserRepository $userRepository)
    {
        // Id del usuario iniciado
        $authUserId = $userRepository->authUser('id');

        // Usuario
        $user = User::findOrFail($authUserId);

        return view('usuarios.perfil',
            compact(
                'user'
            )
        );
    }

    /**
     * Envia los datos del perfil del usuario
     *
     * @param ProfileRequest $request
     * @param UserRepository $userRepository
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateUserAuth(ProfileRequest $request, UserRepository $userRepository)
    {
        // Id del usuario iniciado
        $authUserId = $userRepository->authUser('id');

        // Datos
        $data = $request->input();

        // Crea el nuevo role
        $user = User::findOrFail($authUserId);
        $user->update($data);

        if ($request->hasFile('avatar')
            && $request->file('avatar')->isValid()) {

            $ubicacion = storage_path('app/usuarios/' . $user->id . '/');

            if (!file_exists($ubicacion) || !is_dir($ubicacion)) {
                mkdir($ubicacion, 0775, 1);

            }
            //Nombre del archivo con la extension
            $archivoOriginal = 'avatarOriginal' . '.' . $request->file('avatar')->getClientOriginalExtension();

            //Mueve el archivo a la carpeta elegida
            $request->file('avatar')->move($ubicacion,$archivoOriginal);

            //Combierte la imagen
            $img = Image::make($ubicacion.$archivoOriginal)->resize(300, 300, function($constraint) {
                $constraint->aspectRatio();
            });

            //Nombre del archivo final
            $archivo = 'avatar' . '.' . $request->file('avatar')->getClientOriginalExtension();
            $img->save($ubicacion.$archivo);

            // Guarda el nombre del archivo en la aplicacion
            $user->avatar = $archivo;
            $user->save();
        }

        return redirect('/home');
    }

    /**
     * Descarga la imagen del usuario iniciado
     *
     * @param UserRepository $userRepository
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadAvatar(UserRepository $userRepository)
    {
        // Id del usuario iniciado
        $authUserId = $userRepository->authUser('id');

        //Avatar
        $userAvatar = $userRepository->authUser('avatar');

        $archivo = storage_path('app/usuarios/' . $authUserId . '/' . $userAvatar );
        $extension = pathinfo(($archivo), PATHINFO_EXTENSION);

        return response()->download($archivo, $userAvatar . '.' . $extension);
    }

    /**
     * Elimina la imagen del perfil del usuario iniciado
     *
     * @param UserRepository $userRepository
     * @return RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteAvatar(UserRepository $userRepository)
    {
        // Id del usuario iniciado
        $authUserId = $userRepository->authUser('id');

        // Busca al usuario y elimina su avatar
        $user = User::findOrFail($authUserId);
        $user->avatar = null;
        $user->save();

        return redirect('usuarios/perfil');
    }
}
