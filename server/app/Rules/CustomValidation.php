<?php

namespace App\Rules;

use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Validator;

class CustomValidation extends Validator {

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return bool
     */
    protected function validateUniqueNotDeleted($attribute, $value, $parameters)
    {
        /*
        Busca si existe en la base de datos
        donde el campo deleted_at es un campo vacio
        */
        $result = DB::table($parameters[0])->where(function($query) use ($attribute, $value, $parameters) {
            // Donde $attribute es = al nombre del campo
            // y $value es el valor del campo
            $query->where($attribute, 'ilike', $value)
            ->whereNull('deleted_at');
        })->first();

        if (isset($parameters[3])) {
            return true;
        }
        // Retorna $result en un dato booleano
        return $result ? false : true;
    }
}
