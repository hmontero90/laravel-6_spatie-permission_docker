<?php

namespace App;

class UserRepository extends User
{
    /**
     * Usuario iniciado
     * En caso de especificar "columnName"
     * retorna el usuario con su respectiva columna
     *
     * @param null $columnName
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public static function authUser($columnName = null)
    {
        switch ($columnName){
            case $columnName:
                $authUser = auth()->user()->$columnName;
                break;
            default:
                $authUser = auth()->user();
                break;
        }

        return $authUser;
    }

    /**
     * Retorna el Role del usuario iniciado.
     * En caso de especificar "columnName"
     * retorna el role con su respectiva columna
     *
     * @param null $columnName
     * @return mixed
     */
    public static function authUserRole($columnName = null)
    {
        switch ($columnName){
            case $columnName:
                $authUserRole = auth()->user()->roles->first()->$columnName;
                break;
            default:
                $authUserRole = auth()->user()->roles->first();
                break;
        }

        return $authUserRole;
    }

    /**
     * Retorna el Modelo de Usuario segun los Roles que tenga
     *
     * @param $userModel
     * @return mixed
     */
    public static function userRoleCase($userModel)
    {
        // Nombre del role del usuario iniciado
        $authUserRoleName = UserRepository::authUserRole('name');

        // Casos del usuario segun su role
        switch ($authUserRoleName) {
            case $authUserRoleName === 'super-admin':
                break;
            case $authUserRoleName === 'admin':
                $userModel->whereHas('roles',function ($q){
                    $q->whereNotIn('name', ['super-admin']);
                });
                break;
            default:
                $userModel->whereHas('roles',function ($q) use ($authUserRoleName){
                    $q->where('name', '=', $authUserRoleName);
                });
                break;
        }

        return $userModel;
    }
}
