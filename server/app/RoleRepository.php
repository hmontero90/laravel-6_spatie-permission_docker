<?php

namespace App;

class RoleRepository extends Role
{

    /**
     * Devuelve los roles del usuario
     *
     * @return mixed
     */
    public static function roleAccordingToUser()
    {
        // Verifica si sos super-admin o admin y le muestra todos los roles
        // si no solo trae lZos roles del usuario.
        $authUser = new UserRepository();
        $authUserRoleName = $authUser->authUserRole('name');
        switch ($authUserRoleName) {
            case $authUserRoleName === 'super-admin':
                $roles = Role::orderBy('id', 'ASC')
                    ->get();
                break;
            case $authUserRoleName === 'admin':
                $roles = Role::whereNotIn('name', ['super-admin'])
                    ->orderBy('id', 'ASC')
                    ->get();
                break;
            default:
                $roles = Role::where('id', $authUser->authUserRole('id'))
                    ->orderBy('id', 'ASC')
                    ->get();
                break;
        }

        return $roles;
    }

    /**
     * Recibe el modelo de Role
     * y lo retorna segun los permisos del usuarios
     * ordenados por id de forma asendiente
     *
     * @param $roleModel
     * @return mixed
     */
    public static function rolesPermissionsCase($roleModel)
    {
        // Role del usuario iniciado
        $userRepository = new UserRepository;
        $userRoleName = $userRepository->authUserRole('name');

        // Muestra roles segun corresponda
        switch ($userRoleName) {
            case $userRoleName === 'super-admin':
                $roles = $roleModel->orderBy('id', 'ASC');
                break;
            case $userRoleName === 'admin':
                $roles = $roleModel->whereNotIn('name', ['super-admin'])
                    ->orderBy('id', 'ASC');
                break;
            default:
                $roles = $roleModel->where('name', '=', $userRoleName);
                break;
        }

        return $roles;
    }

    public static function userPermissionsCase()
    {
        // Role del usuario iniciado
        $userRepository = new UserRepository;
        $userRoleName = $userRepository->authUserRole('name');
        switch ($userRoleName) {
            case $userRoleName == 'super-admin'
                || $userRoleName == 'admin':
                $permissions = Permission::all();
                break;
            default:
                $permissions = Permission::whereIn('id', $userRepository->authUserRole('permissions')
                    ->pluck('id')
                )->get();
                break;
        }

        return $permissions;
    }
}
