<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleHasPermissions extends Model
{
    /**
     * Nombre de la tabla en base de datos
     *
     * @var string
     */
    protected $table = 'role_has_permissions';

    /**
     * Campos rellenable
     *
     * @var array
     */
    protected $fillable = [
        'permission_id',
        'role_id'
    ];

    public function permissions()
    {
        return  $this->belongsTo(Permission::class,'permission_id');
    }
}
