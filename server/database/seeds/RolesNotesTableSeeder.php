<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesNotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Crea el role administrador y agrega todos los permisos hasta el momento
        $roleAdmin = Role::create([
            'name' => 'notes',
            'display_name' => 'Notas',
            'level' => 3
        ]);
        $roleAdmin->givePermissionTo(Permission::where('name', 'ilike', '%_note')
            ->get()
            ->pluck('name'));
    }
}
