<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UsersNotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Role administrador
        $roleNotes = Role::where('name', 'notes')->first();

        factory(App\User::class, 100)
            ->create()
            ->each(function($user) use ($roleNotes) {
                $user->assignRole($roleNotes->name);
            });

//        // Usuario "Notes"
//        $userNotes = Factory(App\User::class)->create([
//            'name' => 'Notas',
//            'password' => bcrypt('educar123'),
//            'email' => 'notas@educar.gov.ar',
//        ])->assignRole($roleNotes->name);

//        // Asigna el usuario "Notes" a la tabla users
//        $userNotes->update([
//            'created_by_user_id' => $userNotes->id,
//            'updated_by_user_id' => $userNotes->id,
//        ]);

//         //Asigna el role "Notes" al usuario "Notes"
//        $userNotes->assignRole($roleNotes->name);
    }
}
