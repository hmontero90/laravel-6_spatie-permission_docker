<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // Usuarios
        Permission::create([
            'name' => 'view_user',
            'display_name' => 'Ver usuarios'
        ]);
        Permission::create([
            'name' => 'create_user',
            'display_name' => 'Crear usuarios'
        ]);
        Permission::create([
            'name' => 'edit_user',
            'display_name' => 'Editar usuarios'
        ]);
        Permission::create([
            'name' => 'delete_user',
            'display_name' => 'Eliminar usuarios'
        ]);
        Permission::create([
            'name' => 'restore_user',
            'display_name' => 'Recuperar usuarios'
        ]);
        Permission::create([
            'name' => 'changePassword_user',
            'display_name' => 'Cambiar contraseña de los usuarios'
        ]);

        // Roles
        Permission::create([
            'name' => 'view_role',
            'display_name' => 'Ver roles'
        ]);
        Permission::create([
            'name' => 'create_role',
            'display_name' => 'Crear roles'
        ]);
        Permission::create([
            'name' => 'edit_role',
            'display_name' => 'Editar roles'
        ]);
        Permission::create([
            'name' => 'delete_role',
            'display_name' => 'Eliminar roles'
        ]);
        Permission::create([
            'name' => 'restore_role',
            'display_name' => 'Recuperar roles'
        ]);
    }
}
