<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Role super administrador
        $roleSuperAdmin = Role::where('name', 'super-admin')->first();

        // Role administrador
        $roleAdmin = Role::where('name', 'admin')->first();

        // Usuario super administrador
        $userSuperAdmin = Factory(App\User::class)->create([
            'name' => 'Super Administrador',
            'password' => bcrypt('educar123'),
            'email' => 'superadmin@educar.gov.ar'
        ]);
        // Asigna el usuario "Super Administrador" a la tabla users
        $userSuperAdmin->update([
            'created_by_user_id' => $userSuperAdmin->id,
            'updated_by_user_id' => $userSuperAdmin->id,
        ]);

        // Asigna el role "Super Administrador" al usuario "Super Administrador"
        $userSuperAdmin->assignRole($roleSuperAdmin->name);

        // Le agrega el usuario "Super Administrador" a la tabla de roles
        $roleSuperAdmin->update([
            'created_by_user_id' => $roleSuperAdmin->id,
            'updated_by_user_id' => $roleSuperAdmin->id,
        ]);

        // Usuario "Administrador"
        $userAdmin = Factory(App\User::class)->create([
            'name' => 'Administrador',
            'password' => bcrypt('educar123'),
            'email' => 'admin@educar.gov.ar',
        ]);
        // Asigna el usuario "Administrador" a la tabla users
        $userAdmin->update([
            'created_by_user_id' => $userAdmin->id,
            'updated_by_user_id' => $userAdmin->id,
        ]);
        // Asigna el role "Administrador" al usuario "Administrador"
        $userAdmin->assignRole($roleAdmin->name);

        // Le agrega el usuario "Administrador" a la tabla de roles
        $roleAdmin->update([
            'created_by_user_id' => $userAdmin->id,
            'updated_by_user_id' => $userAdmin->id,
        ]);
    }
}
