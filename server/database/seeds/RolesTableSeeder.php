<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Crea el role super-admin
        $roleSuperAdmin = Role::create([
            'name' => 'super-admin',
            'display_name' => 'Super Administrador',
            'level' => 1
        ]);

        // Crea el role administrador y agrega todos los permisos hasta el momento
        $roleAdmin = Role::create([
            'name' => 'admin',
            'display_name' => 'Administrador',
            'level' => 2
        ]);
        // Asigna todos lo permisos creados hasta el momento al usuario "Administrador"
        $roleAdmin->givePermissionTo(Permission::all()->pluck('name'));
    }
}
