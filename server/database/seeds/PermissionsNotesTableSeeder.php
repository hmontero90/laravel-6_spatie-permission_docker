<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsNotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // Usuarios
        Permission::create([
            'name' => 'view_note',
            'display_name' => 'Ver notas'
        ]);
        Permission::create([
            'name' => 'create_note',
            'display_name' => 'Crear notas'
        ]);
        Permission::create([
            'name' => 'edit_note',
            'display_name' => 'Editar notas'
        ]);
        Permission::create([
            'name' => 'delete_note',
            'display_name' => 'Eliminar notas'
        ]);
        Permission::create([
            'name' => 'restore_note',
            'display_name' => 'Recuperar notas'
        ]);
    }
}
