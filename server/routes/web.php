<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Principal
 */
Route::get('/', function () {
    return view('welcome');
});

/**
 * Auth
 */
require base_path().'/routes/app/auth.php';

/**
 * Redireccion a la pantalla home
 */
require base_path().'/routes/app/home.php';

/**
 * Logs del sistema
 */
require base_path().'/routes/app/logs.php';

/**
 * Panel de estado del servicio
 */
require base_path().'/routes/app/health.php';

/**
 *  Notas
 */
require base_path().'/routes/app/notes.php';

/**
 * Roles
 */
require base_path().'/routes/app/roles.php';

/**
 * Usuarios
 */
require base_path().'/routes/app/users.php';
