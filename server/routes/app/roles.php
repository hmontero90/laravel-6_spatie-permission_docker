<?php

use Illuminate\Support\Facades\Route;

/**
 * Roles
 */
Route::group([
    'prefix' => 'roles',
    'as' => 'roles.',
    'middleware' => [
        'auth',
    ]
], function () {

    // Listado
    Route::get('/', [
        'uses'  => 'RolesController@lists',
        'as'    => 'lists',
        'middleware' => 'role_or_permission:super-admin|admin|view_role'
    ]);

    // Listado datos
    Route::get('/datos', [
        'uses'  => 'RolesController@getListsData',
        'as'    => 'data',
        'middleware' => 'role_or_permission:super-admin|admin|view_role'
    ]);

    // Vista crear
    Route::get('/crear', [
        'uses'  => 'RolesController@create',
        'as'    => 'create',
        'middleware' => 'role:super-admin|admin'
    ]);

    // Enviar crear
    Route::post('/crear', [
        'uses'  => 'RolesController@store',
        'as'    => 'store',
        'middleware' => 'role:super-admin|admin'
    ]);

    // Vista crear
    Route::get('/editar/{id}', [
        'uses'  => 'RolesController@edit',
        'as'    => 'edit',
        'middleware' => 'role:super-admin|admin'
    ]);

    // Enviar editar
    Route::post('/editar/{id}', [
        'uses'  => 'RolesController@update',
        'as'    => 'update',
        'middleware' => 'role:super-admin|admin'
    ]);

    // Vista Ver
    Route::get('/ver/{id}', [
        'uses'  => 'RolesController@show',
        'as'    => 'show',
        'middleware' => 'role_or_permission:super-admin|admin'
    ]);

    // Eliminar
    Route::get('/eliminar/{id}', [
        'uses'  => 'RolesController@destroy',
        'as'    => 'destroy',
        'middleware' => 'role:super-admin|admin'
    ]);

    // Restaurar
    Route::get('/restaurar/{id}', [
        'uses'  => 'RolesController@restore',
        'as'    => 'restore',
        'middleware' => 'role_or_permission:super-admin|admin'
    ]);

    // Listado papelera
    Route::get('/papelera', [
        'uses'  => 'RolesController@listsTrash',
        'as'    => 'list',
        'middleware' => 'role_or_permission:super-admin|admin|view_role'
    ]);

    // Listado papelera datos
    Route::get('/papelera/datos', [
        'uses'  => 'RolesController@getListsTrashData',
        'as'    => 'data',
        'middleware' => 'role_or_permission:super-admin|admin|view_role'
    ]);
});
