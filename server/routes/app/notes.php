<?php

use Illuminate\Support\Facades\Route;

/**
 *  Notas
 */
Route::group([
    'prefix' => 'notas',
    'as' => 'notes.',
    'middleware' => [
        'auth'
    ]
], function () {

    // Listado
    Route::get('/', [
        'uses'  => 'NotesController@lists',
        'as'    => 'lists',
        'middleware' => 'role_or_permission:super-admin|admin|view_note'
    ]);

    // Listado datos
    Route::get('/datos', [
        'uses'  => 'NotesController@getListsData',
        'as'    => 'data',
        'middleware' => 'role_or_permission:super-admin|admin|view_note'
    ]);

    // Vista crear
    Route::get('/crear', [
        'uses'  => 'NotesController@create',
        'as'    => 'create',
        'middleware' => 'role:super-admin|admin|create_note'
    ]);

    // Enviar crear
    Route::post('/crear', [
        'uses'  => 'NotesController@store',
        'as'    => 'store',
        'middleware' => 'role:super-admin|admin|create_note'
    ]);

    // Vista crear
    Route::get('/editar/{id}', [
        'uses'  => 'NotesController@edit',
        'as'    => 'edit',
        'middleware' => 'role:super-admin|admin|edit_note'
    ]);

    // Enviar editar
    Route::post('/editar/{id}', [
        'uses'  => 'NotesController@update',
        'as'    => 'update',
        'middleware' => 'role:super-admin|admin|edit_note'
    ]);

    // Vista Ver
    Route::get('/ver/{id}', [
        'uses'  => 'NotesController@show',
        'as'    => 'show',
        'middleware' => 'role_or_permission:super-admin|admin|view_note'
    ]);

    // Eliminar
    Route::get('/eliminar/{id}', [
        'uses'  => 'NotesController@destroy',
        'as'    => 'destroy',
        'middleware' => 'role:super-admin|admin|delete_note'
    ]);

    // Restaurar
    Route::get('/restaurar/{id}', [
        'uses'  => 'NotesController@restore',
        'as'    => 'restore',
        'middleware' => 'role_or_permission:super-admin|admin|restore_note'
    ]);

    // Listado papelera
    Route::get('/papelera', [
        'uses'  => 'NotesController@listsTrash',
        'as'    => 'list',
        'middleware' => 'role_or_permission:super-admin|admin|restore_note'
    ]);

    // Listado papelera datos
    Route::get('/papelera/datos', [
        'uses'  => 'NotesController@getListsTrashData',
        'as'    => 'data',
        'middleware' => 'role_or_permission:super-admin|admin|restore_note'
    ]);
});
