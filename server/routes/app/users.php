<?php

use Illuminate\Support\Facades\Route;

/**
 * Usuarios
 */
Route::group([
    'prefix' => 'usuarios',
    'as' => 'users.',
    'middleware' => [
        'auth'
    ]
], function () {
    // Listado
    Route::get('/', [
        'uses'  => 'UsersController@lists',
        'as'    => 'list',
        'middleware' => 'role_or_permission:super-admin|admin|view_user'
    ]);

    // Listado datos
    Route::get('/datos', [
        'uses'  => 'UsersController@getListsData',
        'as'    => 'data',
        'middleware' => 'role_or_permission:super-admin|admin|view_user'
    ]);

    // Vista Ver
    Route::get('/ver/{id}', [
        'uses'  => 'UsersController@show',
        'as'    => 'show',
        'middleware' => 'role_or_permission:super-admin|admin|view_user'
    ]);

    // Vista crear
    Route::get('/crear', [
        'uses'  => 'UsersController@create',
        'as'    => 'create',
        'middleware' => 'role_or_permission:super-admin|admin|create_user'
    ]);

    // Enviar crear
    Route::post('/crear', [
        'uses'  => 'UsersController@store',
        'as'    => 'store',
        'middleware' => 'role_or_permission:super-admin|admin|create_user'
    ]);

    // Vista editar
    Route::get('/editar/{id}', [
        'uses'  => 'UsersController@edit',
        'as'    => 'edit',
        'middleware' => 'role_or_permission:super-admin|admin|edit_user'
    ]);

    // Enviar crear
    Route::post('/editar/{id}', [
        'uses'  => 'UsersController@update',
        'as'    => 'update',
        'middleware' => 'role_or_permission:super-admin|admin|edit_user'
    ]);

    // Eliminar
    Route::get('/eliminar/{id}', [
        'uses'  => 'UsersController@destroy',
        'as'    => 'destroy',
        'middleware' => 'role_or_permission:super-admin|admin|delete_user'
    ]);

    // Restaurar
    Route::get('/restaurar/{id}', [
        'uses'  => 'UsersController@restore',
        'as'    => 'restore',
        'middleware' => 'role_or_permission:super-admin|admin|restore_user'
    ]);

    // Listado papelera
    Route::get('/papelera', [
        'uses'  => 'UsersController@listsTrash',
        'as'    => 'list',
        'middleware' => 'role_or_permission:super-admin|admin|view_user'
    ]);

    // Listado papelera datos
    Route::get('/papelera/datos', [
        'uses'  => 'UsersController@getListsTrashData',
        'as'    => 'data',
        'middleware' => 'role_or_permission:super-admin|admin|view_user'
    ]);

    // Vista de cambiar contraseña propia
    Route::get('/cambiar/contrasenia', [
        'uses'  => 'UsersController@changePasswordUserAuth',
        'as'    => 'changePasswordUserAuth',
    ]);

    // Cambiar contraseña propia
    Route::post('/cambiar/contrasenia', [
        'uses'  => 'UsersController@newPasswordUserAuth',
        'as'    => 'newPasswordUserAuth',
    ]);

    // Vista de cambiar contraseña
    Route::get('/cambiar/contrasenia/{id}', [
        'uses'  => 'UsersController@changePassword',
        'as'    => 'changePassword',
        'middleware' => 'role_or_permission:super-admin|admin|changePassword_user'
    ]);

    // Cambiar contraseña
    Route::post('/cambiar/contrasenia/{id}', [
        'uses'  => 'UsersController@newPassword',
        'as'    => 'newPassword',
        'middleware' => 'role_or_permission:super-admin|admin|changePassword_user'
    ]);

    // Vista editar
    Route::get('/perfil', [
        'uses'  => 'UsersController@editUserAuth',
        'as'    => 'editUserAuth',
    ]);

    // Enviar crear
    Route::post('/perfil', [
        'uses'  => 'UsersController@updateUserAuth',
        'as'    => 'updateUserAuth',
    ]);

    // Descarga la imagen del perfil
    Route::get('/perfil/avatar', [
        'uses'  => 'UsersController@downloadAvatar',
        'as'    => 'downloadAvatar',
    ]);

    // Eliminar la imagen del perfil
    Route::get('/perfil/eliminar/avatar', [
        'uses'  => 'UsersController@deleteAvatar',
        'as'    => 'deleteAvatar',
    ]);
});
