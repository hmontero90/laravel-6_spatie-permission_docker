<?php

use Illuminate\Support\Facades\Route;

/**
 * Panel de estado del servicio
 */
Route::group([
    'namespace' => '\PragmaRX\Health\Http\Controllers',
    'prefix' => 'health',
    'as' => ' pragmarx.health.',
    'middleware' => [
        'auth'
    ]
], function () {
    // Panel
    Route::get('panel', [
        'uses' => 'Health@panel',
        'as' => 'panel',
        'middleware' => 'role:super-admin|admin'
    ]);

    // Assets.js
    Route::get('assets/css/app.css', [
        'uses' => 'Health@assetAppCss',
        'as' => 'assets.js',
        'middleware' => 'role:super-admin|admin'
    ]);

    // js app.js
    Route::get('assets/js/app.js', [
        'uses' => 'Health@assetAppJs ',
        'as' => 'assets.js',
        'middleware' => 'role:super-admin|admin'
    ]);

    // Check
    Route::get('check', [
        'uses' => 'Health@check',
        'as' => 'check',
        'middleware' => 'role:super-admin|admin'
    ]);

    // Config
    Route::get('config', [
        'uses' => 'Health@config',
        'as' => 'config',
        'middleware' => 'role:super-admin|admin'
    ]);

    // Resources
    Route::get('resources', [
        'uses' => 'Health@allResources',
        'as' => 'resources.all',
        'middleware' => 'role:super-admin|admin'
    ]);

    // Resources get
    Route::get('resources/{slug}', [
        'uses' => 'Health@getResource',
        'as' => 'resources.get',
        'middleware' => 'role:super-admin|admin'
    ]);

    // String
    Route::get('string', [
        'uses' => 'Health@string',
        'as' => 'string',
        'middleware' => 'role:super-admin|admin'
    ]);
});
