<?php

use Illuminate\Support\Facades\Route;

/**
 * Auth
 */
Route::group([
    'namespace' => '\App\Http\Controllers\Auth',
], function () {
    // Login
    Route::get('login', [
        'uses' => 'LoginController@showLoginForm'
    ]);

    // Login
    Route::get('auth/login', [
        'uses' => 'LoginController@showLoginForm'
    ]);

    // Login post
    Route::post('login', [
        'uses' => 'LoginController@login',
        'as' => 'login'
    ]);

    // Logout
    Route::get('logout', [
        'uses' => 'LoginController@logout'
    ]);

    // Logout post
    Route::post('logout', [
        'uses' => 'LoginController@logout',
        'as' => 'logout'
    ]);

    /*
     * Password
     */
    Route::group([
        'prefix' => 'password',
        'as' => 'password.'
    ], function () {
        // Password email
        Route::post('email', [
            'uses' => 'ForgotPasswordController@sendResetLinkEmail',
            'as' => 'email'
        ]);

        // Password reset
        Route::get('reset', [
            'uses' => 'ForgotPasswordController@showLinkRequestForm',
            'as' => 'request'
        ]);

        // Password reset post
        Route::post('reset', [
            'uses' => 'ResetPasswordController@reset',
            'as' => 'update'
        ]);

        // Password reset token
        Route::get('reset/{token}', [
            'uses' => 'ResetPasswordController@showResetForm',
            'as' => 'reset'
        ]);
    });

    /*
     * Register
     */
    Route::group([
        'prefix' => 'register',
    ], function (){
        // Register post
        Route::post('/', [
            'uses' => 'RegisterController@register',
        ]);

        // Register
        Route::get('/', [
            'uses' => 'RegisterController@showRegistrationForm',
            'as' => 'register'
        ]);
    });

    /*
     * Middleware Auth
     */
    Route::group([
        'middleware' => [
            'auth'
        ]
    ], function () {
        // Password
        Route::group([
            'prefix' => 'password'
        ], function () {
            // Password confirm
            Route::get('confirm', [
                'uses' => 'ConfirmPasswordController@showConfirmForm',
                'as' => 'password.confirm'
            ]);

            // Password confirm post
            Route::post('confirm', [
                'uses' => 'ConfirmPasswordController@confirm'
            ]);
        });
    });
});
