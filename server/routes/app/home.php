<?php

use Illuminate\Support\Facades\Route;

/**
 * Redireccion a la pantalla home
 */
Route::get('/home', 'HomeController@index')->name('home');
