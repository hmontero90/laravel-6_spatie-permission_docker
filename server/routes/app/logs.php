<?php

use Illuminate\Support\Facades\Route;

/**
 * Logs del sistema
 */
Route::group([
    'namespace' => '\Rap2hpoutre\LaravelLogViewer',
    'prefix' => 'logs',
    'as' => 'logs.',
    'middleware' => [
        'auth'
    ]
], function () {
    // listado de los logs
    Route::get('/', [
        'uses' => 'LogViewerController@index',
        'as' => 'index',
        'middleware' => 'role:super-admin|admin'
    ]);
});
