# Proyecto laravel-6

Paquetes agregados de la aplicación:

* Docker => docker-compose => https://docs.docker.com/compose/
* Laravel Versión => laravel/framework 6 => https://laravel.com/docs/6.x
* Autenticación => laravel/ui => https://github.com/laravel/ui
* Roles y Permisos => spatie/laravel-permission => https://github.com/spatie/laravel-permission
* Ver Logs => rap2hpoutre/laravel-log-viewer => https://github.com/rap2hpoutre/laravel-log-viewer
* Panel de estado del servicio => pragmarx/health => https://packagist.org/packages/pragmarx/health
* Depuración => barryvdh/laravel-debugbar => https://github.com/barryvdh/laravel-debugbar
* Tablas Personalizadas => yajra/laravel-datatables => https://github.com/yajra/laravel-datatables
* Manejo de imagenes => intervention/image => https://github.com/Intervention/image

Pasos para levantar la aplicación:

* Instalar docker-compose => https://docs.docker.com/compose/install/
* Iniciar aplicación => docker-compose up -d
* Entramos al docker-compose del php => docker-compose exec "NOMBRE_DEL_DOCKER_COMPOSE_PHP" bash
* Acceder a la carpeta "/server"
* composer install

Comandos útiles:
* Crea y inicia contenedores del docker docker-compose => docker-compose up
* Ejecuta docker-compose en segundo plano => docker-compose up -d 
* Crear imagen docker-compose antes de ser iniciado => docker-compose up --build
* Acceder al docker docker-compose exec "NOMBRE_DEL_DOCKER_COMPOSE_PHP" bash
* Apagar docker-compose => docker-compose down
* Destruir docker-compose => docker-compose down -v
* Reconstruir app.js modo dev => /server/npm run dev
* Reconstruir app.js modo produccion => /server/npm run production
* Reconstruir app.js corriendo y recompilando en modo activo => /server/npm run watch